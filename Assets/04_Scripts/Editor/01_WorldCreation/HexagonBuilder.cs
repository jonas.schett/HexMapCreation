﻿#if (UNITY_EDITOR)
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class HexagonBuilder : EditorWindow
{
    private struct TriangleVectors
    {
        public TriangleVectors(TriangleVectors other, Vector3 addOn)
        {
            one = other.one + addOn;
            two = other.two + addOn;
            three = other.three + addOn;
        }

        public TriangleVectors(Vector3 one, Vector3 two, Vector3 three)
        {
            this.one = one;
            this.two = two;
            this.three = three;
        }

        public Vector3 one;
        public Vector3 two;
        public Vector3 three;
    }

    [MenuItem("Window/HexMapTools/HexagonBuilder")]
    public static void ShowWindow()
    {
        GetWindow(typeof(HexagonBuilder));
    }

    private void OnGUI()
    {
        outerRadius = EditorGUI.IntField(new Rect(3, 3, position.width - 6, 20), "Radius", outerRadius);
        n = EditorGUI.IntField(new Rect(3, 26, position.width - 6, 20), "iterations", n);
        noSharedVerts = EditorGUI.Toggle(new Rect(3, 49, position.width - 6, 20), "No shared vertices", noSharedVerts);
        showDebugginInfo = EditorGUI.Toggle(new Rect(3, 72, position.width - 6, 20), "Show debugging info", showDebugginInfo);
        floatCompareDelta = EditorGUI.FloatField(new Rect(3, 95, position.width - 6, 20), "Float comparison delta", floatCompareDelta);
        rotatedHexagon = EditorGUI.Toggle(new Rect(3, 118, position.width - 6, 20), "Vertex north orientation", rotatedHexagon);
        if (n != 0 && outerRadius != 0)
        {
            if (GUI.Button(new Rect(3, 141, position.width - 6, 20), "Build Hexagon"))
            {
                Generate();
            }
        }
    }

    private int n = 1;
    private int outerRadius = 10;
    private bool showDebugginInfo;
    private float floatCompareDelta = 0.1f;

    private static float hexDiagonalMultiplier = 0.86602540378444f;

    private Vector3 A;
    private Vector3 B;
    private Vector3 C;
    private Vector3 D;
    private Vector3 E;
    private Vector3 F;

    private Vector3[] baseVectors;

    private Vector3[] vertices;
    private int[] triangles;

    private bool noSharedVerts = true;

    private bool rotatedHexagon = false;

    private List<List<List<TriangleVectors>>> triangleVectorList;

    Dictionary<Vector3, int> vectorMap;

    private void Generate()
    {
        GenerateBaseVectors();
        GenerateVertices();
        GenerateTriangleVectors();
        GenerateTriangleMapping();
        CreateMesh();
    }

    private void CreateMesh()
    {
        Mesh mesh = new Mesh();

        if (noSharedVerts)
        {
            Vector3[] unsharedVertices = new Vector3[triangles.Length];
            int[] unsharedTriangles = triangles;
            for (int i = 0; i < triangles.Length; i++)
            {
                unsharedVertices[i] = vertices[triangles[i]];
                unsharedTriangles[i] = i;
            }
            mesh.vertices = unsharedVertices;
            mesh.triangles = unsharedTriangles;
        }
        else
        {
            mesh.vertices = vertices;
            mesh.triangles = triangles;
        }

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        mesh.name = string.Format("hexMesh{0}_{1}", mesh.vertices.Length, mesh.triangles.Length / 3);
        string hexName = string.Format("HexagonRadius{0}_v{1}_t{2}", outerRadius, mesh.vertices.Length, mesh.triangles.Length / 3);

        GameObject gameObject = new GameObject();
        gameObject.transform.position = Vector3.zero;
        gameObject.AddComponent<MeshFilter>();

        MeshFilter mf = gameObject.GetComponent<MeshFilter>();
        mf.sharedMesh = mesh;

        gameObject.AddComponent<MeshRenderer>();
        gameObject.GetComponent<MeshRenderer>().material = (Material)Resources.Load("TerrainMaterial", typeof(Material));
        gameObject.AddComponent<Hexagon>();
        gameObject.name = hexName;

        gameObject.GetComponent<Hexagon>().FirstEdgeVertexIndex = GetEdgeVertexIndices(n-1) + 1;
        gameObject.GetComponent<Hexagon>().HexagonIterations = n;

        string path = EditorUtility.SaveFilePanel("Save new Hexagon as prefab", "Assets/", hexName, "asset");
        path = FileUtil.GetProjectRelativePath(path);
        AssetDatabase.CreateAsset(mf.sharedMesh, path);
        AssetDatabase.SaveAssets();
    }

    public static void SaveMesh(Mesh mesh, string name, bool makeNewInstance, bool optimizeMesh)
    {
        string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/Resources/Meshes", name, "asset");
        if (string.IsNullOrEmpty(path)) return;

        path = FileUtil.GetProjectRelativePath(path);

        Mesh meshToSave = (makeNewInstance) ? Object.Instantiate(mesh) as Mesh : mesh;

        if (optimizeMesh)
            MeshUtility.Optimize(meshToSave);

        AssetDatabase.CreateAsset(meshToSave, path);
        AssetDatabase.SaveAssets();
    }

    private int GetVertexAmount(int n)
    {
        return n == 0 ? 1 : 6 * n + GetVertexAmount(n - 1);
    }

    private void GenerateVertices()
    {
        List<Vector3> vertexList = new List<Vector3>();
        vertexList.Add(Vector3.zero);
        for (int i = 1; i <= n; i++)
        {
            for (var j = 0; j < baseVectors.Length; j++)
            {
                int vectorIndex = (j + 2) >= baseVectors.Length ? j + 2 - baseVectors.Length : j + 2;
                for (int k = 0; k < i; k++)
                {
                    vertexList.Add((i * baseVectors[j]) + (k * baseVectors[vectorIndex]));
                }
            }
        }
        vertices = vertexList.ToArray();
        /*
        if(Selection.gameObjects.Length > 0)
        {
            Selection.gameObjects[0].GetComponent<HexGizmoDrawer>().vertices = vertexList;
        }*/
    }
    public static void GetEdgeVertexIndexRange(out int begin, out int end, int iterations)
    {
        begin = GetEdgeVertexIndices(iterations - 1) + 1;
        end = GetEdgeVertexIndices(iterations);
    }
    public static int GetEdgeVertexIndices(int iterations)
    {
        if (iterations <= 0)
        {
            return 0;
        }
        else
        {
            return 6 * iterations + GetEdgeVertexIndices(iterations - 1);
        }

    }
    private void GenerateTriangleMapping()
    {
        List<int> triangleMapping = new List<int>();
        foreach (var topList in triangleVectorList)
        {
            foreach (var midList in topList)
            {
                foreach (var triangleVector in midList)
                {
                    triangleMapping.Add(GetVertexIndex(triangleVector.one));
                    triangleMapping.Add(GetVertexIndex(triangleVector.two));
                    triangleMapping.Add(GetVertexIndex(triangleVector.three));
                }
            }
        }
        triangles = triangleMapping.ToArray();
    }

    private int GetVertexIndex(Vector3 input)
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            float x = vertices[i].x;
            float y = vertices[i].y;
            float z = vertices[i].z;
            if ((MUtils.CompareFloats(x, input.x, floatCompareDelta) 
                && MUtils.CompareFloats(y, input.y, floatCompareDelta) 
                && MUtils.CompareFloats(z, input.z, floatCompareDelta)) 
                || input == vertices[i])
            {
                return i;
            }
        }
        return 2;
    }

    private void GenerateTriangleVectors()
    {
        triangleVectorList = new List<List<List<TriangleVectors>>>();
        for (int i = 0; i < n; i++)
        {
            triangleVectorList.Add(new List<List<TriangleVectors>>());
            for (int j = 0; j < baseVectors.Length; j++)
            {
                int vectorPlusOneIndex = (j + 1) >= baseVectors.Length ? j + 1 - baseVectors.Length : j + 1;
                triangleVectorList[i].Add(new List<TriangleVectors>());
                if (i == 0)
                {
                    triangleVectorList[i][j].Add(new TriangleVectors { one = Vector3.zero, two = baseVectors[j], three = baseVectors[vectorPlusOneIndex] });
                }
                else
                {
                    for (int k = 0; k < i * 2 - 1; k++)
                    {

                        triangleVectorList[i][j].Add(new TriangleVectors(triangleVectorList[i - 1][j][k], baseVectors[j]));
                    }
                    int previousLastVectorIndex = triangleVectorList[i - 1][j].Count - 1;
                    triangleVectorList[i][j].Add(new TriangleVectors(triangleVectorList[i - 1][vectorPlusOneIndex][0], baseVectors[j]));
                    triangleVectorList[i][j].Add(new TriangleVectors(triangleVectorList[i - 1][j][previousLastVectorIndex], baseVectors[vectorPlusOneIndex]));
                }
            }
        }
    }

    private void GenerateBaseVectors()
    {
        float section = (float)outerRadius / (float)n;

        if(rotatedHexagon)
        {
            // if we want to have a vertex facing north
            A = new Vector3(0f, 0f, section);
            B = new Vector3(hexDiagonalMultiplier * section, 0f, section / 2f);
            C = new Vector3(hexDiagonalMultiplier * section, 0f, -section / 2f);
        }
        else
        {
            // if we want to have a side facing north
            A = new Vector3(section / 2f, 0, hexDiagonalMultiplier * section);
            B = new Vector3(section, 0, 0);
            C = new Vector3(section / 2f, 0, -(hexDiagonalMultiplier * section));
        }

        D = -A;
        E = -B;
        F = -C;

        baseVectors = new[] { A, B, C, D, E, F };
    }

    void OnSceneGUI(SceneView sceneView)
    {
        if (showDebugginInfo && vertices != null && vertices.Length > 0)
        {
            int counter = 0;
            foreach (var vert in vertices)
            {
                var vertexAngle = NumberConversions.Rad2Deg(Mathf.Atan2(vert.z, vert.x));
                Handles.color = Color.blue;
                Handles.Label(vert + Vector3.up, counter++.ToString() + "\n" + vertexAngle.ToString());
            }
        }


    }

    private void OnFocus()
    {
        SceneView.duringSceneGui -= OnSceneGUI;

        SceneView.duringSceneGui += OnSceneGUI;
    }

    private void OnDestroy()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
    }

}
#endif