﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuadraticBezier
{
    public Vector3 Point0;
    public Vector3 Point1;
    public Vector3 Point2;

    public QuadraticBezier(QuadraticBezier other, Vector3 offset)
    {
        Point0 = other.Point0 + offset;
        Point1 = other.Point1 + offset;
        Point2 = other.Point2 + offset;
    }
    public QuadraticBezier(QuadraticBezier other) : this(other.Point0, other.Point1, other.Point2) { }

    public QuadraticBezier(Vector3 p0, Vector3 p1, Vector3 p2)
    {
        Point0 = p0;
        Point1 = p1;
        Point2 = p2;
    }
    public Vector3 GetPoint(float t)
    {
        t = Mathf.Clamp01(t);
        var p1 = Vector3.Lerp(Point0, Point1, t);
        var p2 = Vector3.Lerp(Point1, Point2, t);
        return Vector3.Lerp(p1, p2, t);
    }

    public float GetTopDownDistanceFromCurve(Vector3 point, float granularity, ref float usedF)
    {
        float distance;
        float minDistance = float.MaxValue;
        for (float f = 0f; f <= 1f; f += granularity)
        {
            Vector3 pos = GetPoint(f);
            distance = GetTopDownDistance(point, pos);
            if (distance < minDistance)
            {
                minDistance = distance;
                usedF = f;
            }
        }
        return minDistance;
    }

    public Vector2 GetNearestPointOnCurve(Vector3 point)
    {
        Vector3 minValue = new Vector3();
        float distance;
        float minDistance = float.MaxValue;
        for(float f = 0f; f<1f; f+=0.05f)
        {
            Vector3 pos = GetPoint(f);
            distance = GetTopDownDistance(point, pos);
            if(distance < minDistance)
            {
                minDistance = distance;
                minValue = pos;
            }
        }

        return minValue;
    }

    private static float GetTopDownDistance(Vector3 a, Vector3 b)
    {
        Vector2 aTopDown = new Vector2(a.x, a.z);
        Vector2 bTopDown = new Vector2(b.x, b.z);
        return (aTopDown - bTopDown).magnitude;
    }

    private static float GetDistance(Vector3 a, Vector3 b)
    {
        return (b - a).magnitude;
    }
}
