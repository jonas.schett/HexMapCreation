﻿#if(UNITY_EDITOR)
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(Hexagon)), CanEditMultipleObjects]
public class HexagonInSceneEditor : Editor
{
    [SerializeField]
    private float riverWidth = 20f;
    [SerializeField]
    private float riverGranularity = 0.025f;
    [SerializeField]
    private float riverDepth = 5f;
    [SerializeField]
    private int riverLength = 5;

    private List<List<Hexagon>> riverPaths;
    private List<Hexagon> selectedPath;
    private List<QuadraticBezier> bezierCurves;

    //List for adjusting vertices
    private List<Vector3[]> meshInformation;

    //List for storing river centre points;
    private List<Vector3>[] finalRiverCentrePoints;

    private List<Vector3>[] finalRiverLocalRightVectors;

    //Dictionary used to display the river in the UI
    private Dictionary<Vector3, Vector3> changedVertices;
    // Start is called before the first frame update
    private void OnSceneGUI()
    {
        DrawRiverPaths();
    }

    void Draw(Hexagon target)
    {
        Handles.color = Color.black;
        Handles.DrawLine(target.transform.position, target.transform.position + Vector3.up * 50);
        foreach(Hexagon neighbour in target.Neighbours.GetArray())
        {
            if(neighbour.ElevationFoundationType <= target.ElevationFoundationType && neighbour.ElevationFoundationType >= target.ElevationFoundationType-2)
            {
                Handles.DrawLine(target.transform.position, neighbour.transform.position);
            }
        }
    }

    void DrawRiverPaths()
    {
        if (selectedPath != null)
        {
            for (int i = 2; i < selectedPath.Count; i++)
            {
                Handles.color = Color.black;
                //Handles.DrawLine(path[i - 1].transform.position, path[i].transform.position);
                Vector3 p1 = (selectedPath[i - 2].transform.position + selectedPath[i - 1].transform.position) / 2f;
                Vector3 p2 = (selectedPath[i - 1].transform.position + selectedPath[i].transform.position) / 2f;
                Handles.CubeHandleCap(0, p1, Quaternion.identity, 10, EventType.Ignore);
                Handles.CubeHandleCap(0, p2, Quaternion.identity, 10, EventType.Ignore);
                Handles.DrawBezier(p2, p1, selectedPath[i - 1].transform.position, selectedPath[i - 1].transform.position, Color.black, null, 1f);
            }
        }

        //if(bezierCurves != null)
        //{
        //    Handles.color = Color.green;
        //    foreach (var curve in bezierCurves)
        //    {
        //        for(float f = 0; f<= 1.0f; f+= 0.1f)
        //        {
        //            Handles.DrawWireCube(curve.GetPoint(f), Vector3.one * 5f);
        //        }
        //    }
        //}

        if (finalRiverCentrePoints != null && finalRiverLocalRightVectors != null && finalRiverCentrePoints.Length == finalRiverLocalRightVectors.Length)
        {
            for(int i = 0; i < finalRiverCentrePoints.Length; i ++)
            {
                for(int j = 0; j < finalRiverCentrePoints[i].Count; j ++)
                {
                    Handles.DrawWireCube(finalRiverCentrePoints[i][j], Vector3.one * 5f);
                    Handles.DrawLine(finalRiverCentrePoints[i][j], finalRiverCentrePoints[i][j] + riverWidth * finalRiverLocalRightVectors[i][j]);
                    Handles.DrawLine(finalRiverCentrePoints[i][j], finalRiverCentrePoints[i][j] - riverWidth * finalRiverLocalRightVectors[i][j]);
                }
            }
        }

        //if(changedVertices != null)
        //{
        //    foreach(var pair in changedVertices)
        //    {
        //        Handles.DrawWireCube(pair.Key, Vector3.one);
        //        Handles.DrawWireCube(pair.Value, Vector3.one);
        //        Handles.DrawLine(pair.Key, pair.Value);
        //    }
        //}
    }

    void CalculateRiverPaths(Hexagon hex, int maxDrop,int desiredPathLength, int pathLength = 0, List<Hexagon> previous = null)
    {
        if(previous == null)
        {
            previous = new List<Hexagon>();
        }
        previous.Add(hex);

        // we don't want to continue if we are above the desired pathlength
        if(pathLength > desiredPathLength)
        {
            return;
        }

        if (hex.ElevationFoundationType == HexagonElevantionFoundationType.Sea)
        {
            if(pathLength == desiredPathLength)
            {
                riverPaths.Add(previous);
            }
            return;
        }

        List<Hexagon> possibleNeighbours = new List<Hexagon>();
        // draw line for each neighbour
        HexagonElevantionFoundationType elevation = HexagonElevantionFoundationType.HighMountain;
        foreach (Hexagon neighbour in hex.Neighbours.GetArray())
        {
            if (neighbour.ElevationFoundationType <= hex.ElevationFoundationType && neighbour.ElevationFoundationType >= hex.ElevationFoundationType - maxDrop)
            {
                possibleNeighbours.Add(neighbour);
                elevation = elevation > neighbour.ElevationFoundationType ? neighbour.ElevationFoundationType : elevation;

            }
        }
        foreach (Hexagon neighbour in possibleNeighbours.Where(h => h.ElevationFoundationType == elevation))
        {
            if (!previous.Contains(neighbour))
            {
                // give it a copy of the current list, as we do not want to add all paths to one list
                CalculateRiverPaths(neighbour, maxDrop, desiredPathLength, pathLength + 1, new List<Hexagon>(previous));
            }
        }
    }

    void CreateQuadraticBezierCurves(List<Hexagon> hexagons)
    {
        bezierCurves = new List<QuadraticBezier>();
        for(int i = 0; i < hexagons.Count; i++)
        {
            Vector3 p0;
            Vector3 p1;
            Vector3 p2;
            if (i == 0)
            {
                p0 = hexagons[i].GetMesh().vertices[0] + hexagons[i].transform.position;
                p1 = (hexagons[i].transform.position * 3f + hexagons[i + 1].transform.position) / 4f;
                p2 = (hexagons[i].transform.position + hexagons[i + 1].transform.position) / 2f;
            }
            else if(i == hexagons.Count -1)
            {
                p0 = (hexagons[i].transform.position + hexagons[i - 1].transform.position) / 2f;
                p1 = (hexagons[i].transform.position * 3f + hexagons[i - 1].transform.position) / 4f;
                p2 = hexagons[i].GetMesh().vertices[0] + hexagons[i].transform.position;
            }
            else
            {
                p0 = (hexagons[i].transform.position + hexagons[i - 1].transform.position) / 2f;
                p1 = hexagons[i].transform.position;
                p2 = (hexagons[i].transform.position + hexagons[i + 1].transform.position) / 2f;
            }
            // look for altitude on start and end position
            p0.y = GetVertexAltitudeForCoordinate(hexagons[i], p0);
            p2.y = GetVertexAltitudeForCoordinate(hexagons[i], p2);

            // mid point is half way in between
            p1.y = (p0.y + p2.y) / 2f;

            bezierCurves.Add(new QuadraticBezier(p0, p1, p2));
        }
    }
    float GetVertexAltitudeForCoordinate(Hexagon hex, Vector3 position)
    {
        Vector3 vertexPosition = position - hex.transform.position;
        Vector3[] vertices = hex.GetMesh().vertices;
        float compareDelta = (hex.GridInfo.OuterRadius / hex.HexagonIterations)*1.1f;
        Vector3 result = vertexPosition;
        int numberOfSamePlaces = 0;

        for (int i = hex.FirstEdgeVertexIndex; i < vertices.Length; i++)
        {
            bool xSame = MUtils.CompareFloats(vertices[i].x, vertexPosition.x, compareDelta);
            bool zSame = MUtils.CompareFloats(vertices[i].z, vertexPosition.z, compareDelta);
            if(xSame && zSame)
            {
                result += vertices[i];
                numberOfSamePlaces++;
            }
        }

        if(numberOfSamePlaces != 0)
        {
            result /= numberOfSamePlaces;
        }
        return result.y;
    }

    void CreateModifiedVertexPositions()
    {
        for(int i = 0; i < riverPaths.Count; i ++)
        {
            if (i == 0)
            {
                // we are at the first hexagon and therefore need to use the beziercurve of 
            }
        }
    }

    void CreateRiverInformation()
    {
        bool arraysSameLength = bezierCurves.Count == meshInformation.Count;
        arraysSameLength &= bezierCurves.Count == selectedPath.Count;

        if(bezierCurves != null && meshInformation != null && selectedPath != null && arraysSameLength)
        {
            changedVertices = new Dictionary<Vector3, Vector3>();
            finalRiverCentrePoints = new List<Vector3>[bezierCurves.Count];

            // below dictionary
            // <float: f value used on curve, List<Tuple<int: index of vertex, float : distance to curve>>: list of vertices at this distance>[]: each curve has its own dictionary
            Dictionary<float, List<Tuple<int, float>>>[] depthIndexDictionary = new Dictionary<float, List<Tuple<int, float>>>[bezierCurves.Count];

            // THe below dictionary is used to calculate the river mesh information, as it records the pooints on the mesh that are closest
            // to the curve, those points must be in the centre and therefore we can use them to create the centrepoint of the river mesh
            Dictionary<float, Tuple<int, float>>[] riverCentrePoints = new Dictionary<float, Tuple<int, float>>[bezierCurves.Count];

            //assign vertices to position on curve and add them to the depthIndexDictionary
            for (int i = 0; i < bezierCurves.Count; i ++)
            {
                depthIndexDictionary[i] = new Dictionary<float, List<Tuple<int, float>>>();
                riverCentrePoints[i] = new Dictionary<float, Tuple<int, float>>();
                finalRiverCentrePoints[i] = new List<Vector3>();

                for(int j = 0; j < meshInformation[i].Length; j ++)
                {
                    Vector3 globalVertexPosition = meshInformation[i][j] + selectedPath[i].transform.position;
                    float usedF = 0f;
                    float distance = bezierCurves[i].GetTopDownDistanceFromCurve(globalVertexPosition, riverGranularity, ref usedF);

                    if(distance < riverWidth)
                    {
                        if(riverCentrePoints[i].ContainsKey(usedF))
                        {
                            if(riverCentrePoints[i][usedF].Item2 > distance)
                            {
                                // set it to the index of the currently closest point to this particular value of f
                                riverCentrePoints[i][usedF] = new Tuple<int, float>(j, distance);
                            }
                        }
                        else
                        {
                            riverCentrePoints[i].Add(usedF, new Tuple<int, float>(j, distance));
                        }

                        if(!depthIndexDictionary[i].ContainsKey(usedF))
                        {
                            depthIndexDictionary[i].Add(usedF, new List<Tuple<int, float>>());
                        }
                        depthIndexDictionary[i][usedF].Add(new Tuple<int, float>(j,distance));
                    }
                }
            }
            // calculate depth
            float deepestPoint = float.MaxValue;
            float depthToUse = riverDepth;
            float depthToWidthRatio = riverDepth / riverWidth;
            for(int i = 0; i < bezierCurves.Count; i ++)
            {
                Dictionary<float, float> depthDictionary = new Dictionary<float, float>();
                // calculate depth to use
                for (float f = 0f; f <= 1f; f += riverGranularity)
                {
                    foreach(Tuple<int,float> value in depthIndexDictionary[i][f])
                    {
                        Vector3 vectorValue = meshInformation[i][value.Item1];
                        if(f == 0f)
                        {
                            // do nothing, we use the same depth as before
                        }
                        else if(vectorValue.y - riverDepth < deepestPoint)
                        {
                            deepestPoint = vectorValue.y - riverDepth;
                            depthToUse = riverDepth;
                        }
                        else
                        {
                            depthToUse = Mathf.Abs(vectorValue.y - deepestPoint);
                        }
                    }
                    depthDictionary.Add(f, depthToUse);
                }
                // apply depth
                for(float f = 0f; f<= 1f; f+= riverGranularity)
                {

                    foreach(Tuple<int,float> value in depthIndexDictionary[i][f])
                    {
                        Vector3 newPosition = meshInformation[i][value.Item1];
                        float distance = value.Item2;
                        //float widthToUse = Mathf.Lerp(riverWidth, depthDictionary[f] / depthToWidthRatio, 0.5f);
                        float depthModifier = -distance / riverWidth + 1f;
                        float depthA = Mathf.Lerp(0, depthDictionary[f], Mathf.Sqrt(depthModifier));
                        float depthB = Mathf.Lerp(0, depthDictionary[f], depthModifier);
                        float depth = Mathf.Lerp(depthB, depthA, depthModifier);
                        //float depth = depthModifier * depthDictionary[i][f];
                        newPosition.y -= depth;

                        if (!changedVertices.ContainsKey(selectedPath[i].transform.position + meshInformation[i][value.Item1]))
                        {
                            changedVertices.Add(selectedPath[i].transform.position + meshInformation[i][value.Item1], selectedPath[i].transform.position + newPosition);
                        }
                        meshInformation[i][value.Item1] = newPosition;
                    }
                }

                // show centre of river for mesh calculation
                for(float f = 0f; f <= 1f; f+=riverGranularity)
                {
                    Vector3 centrePoint = meshInformation[i][riverCentrePoints[i][f].Item1] + selectedPath[i].transform.position;
                    centrePoint.y += riverDepth * 3f / 4f;
                    finalRiverCentrePoints[i].Add(centrePoint);
                }
            }

            //Create River Mesh
            // Calculate forward vector
            finalRiverLocalRightVectors = new List<Vector3>[finalRiverCentrePoints.Length];
            for (int i = 0; i < finalRiverCentrePoints.Length; i ++)
            {
                finalRiverLocalRightVectors[i] = new List<Vector3>();
                Vector3 forward;
                Vector3 right;
                for(int j = 0; j < finalRiverCentrePoints[i].Count; j++)
                {
                    if(j >= finalRiverCentrePoints[i].Count - 5)
                    {
                        if(i == finalRiverCentrePoints.Length - 1)
                        {
                            // we have reached the absolute end and must take the forward vector of the previous one
                            forward = (finalRiverCentrePoints[i][j] - finalRiverCentrePoints[i][j-5]).normalized;
                        }
                        else
                        {
                            // we can use the first entry of the next list here
                            forward = (finalRiverCentrePoints[i + 1][5] - finalRiverCentrePoints[i][j]).normalized;
                        }
                    }
                    else
                    {
                        forward = (finalRiverCentrePoints[i][j+5] - finalRiverCentrePoints[i][j]).normalized;
                    }

                    // local right
                    right = new Vector3(forward.z, 0, -forward.x);
                    finalRiverLocalRightVectors[i].Add(right);
                }
            }
        }
    }

    private void ApplyRiver()
    {
        if(selectedPath.Count == meshInformation.Count)
        {
            for (int i = 0; i < selectedPath.Count; i++)
            {
                selectedPath[i].ApplyVertices(meshInformation[i]);
                selectedPath[i].Recalculate();
            }
            meshInformation.Clear();
            bezierCurves.Clear();
            changedVertices.Clear();
            meshInformation = null;
            bezierCurves = null;
            changedVertices = null;
        }
    }

    private void CreateRiverMesh()
    {
        for (int i  = 0; i < selectedPath.Count; i ++)
        {
            Mesh mesh = new Mesh();

            int riverMeshGranularity = 7;

            Vector3[] vertices = new Vector3[Mathf.CeilToInt(finalRiverCentrePoints[i].Count / (float)riverMeshGranularity) * 2];
            int[] triangles = new int[vertices.Length * 3 - 6];

            for(int j = 0, k  = 0; j < finalRiverLocalRightVectors[i].Count; j += riverMeshGranularity, k += 2)
            {
                if(i == 0 && j == 0)
                {
                    Vector3 back = new Vector3(finalRiverLocalRightVectors[i][j].z, 0f, -finalRiverLocalRightVectors[i][j].x);

                    vertices[k] = finalRiverCentrePoints[i][j] -
                                  riverWidth * finalRiverLocalRightVectors[i][j] +
                                  riverWidth * back -
                                  selectedPath[i].transform.position;

                    vertices[k + 1] = finalRiverCentrePoints[i][j] + 
                                      riverWidth * finalRiverLocalRightVectors[i][j] +
                                      riverWidth * back - 
                                      selectedPath[i].transform.position;
                }
                else if(j >= (finalRiverLocalRightVectors[i].Count - riverMeshGranularity))
                {
                    int index = finalRiverCentrePoints[i].Count - 1;
                    vertices[k] = finalRiverCentrePoints[i][index] - riverWidth * finalRiverLocalRightVectors[i][index] - selectedPath[i].transform.position;
                    vertices[k + 1] = finalRiverCentrePoints[i][index] + riverWidth * finalRiverLocalRightVectors[i][index] - selectedPath[i].transform.position;
                }
                else
                {
                    vertices[k] = finalRiverCentrePoints[i][j] - riverWidth * finalRiverLocalRightVectors[i][j] - selectedPath[i].transform.position;
                    vertices[k + 1] = finalRiverCentrePoints[i][j] + riverWidth * finalRiverLocalRightVectors[i][j] - selectedPath[i].transform.position;
                }
            }

            for(int j = 0, k = 2; j < triangles.Length; j +=6, k += 2)
            {
                triangles[j] = k - 2;
                triangles[j + 1] = k + 1;
                triangles[j + 2] = k - 1;
                triangles[j + 3] = k - 2;
                triangles[j + 4] = k;
                triangles[j + 5] = k + 1;
            }
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();

            selectedPath[i].SetRiver(mesh,new QuadraticBezier(bezierCurves[i], -selectedPath[i].transform.position), riverWidth);
        }
        
    }

    public override void OnInspectorGUI()
    {
        var hexagon = (Hexagon)target;
        base.OnInspectorGUI();
        if(hexagon.GridInfo != null && hexagon.Neighbours != null)
        {
            EditorGUILayout.LabelField("Grid Location main", hexagon.GridInfo.GridLocation.ToString());
            foreach (var hex in hexagon.Neighbours.GetArray())
            {
                EditorGUILayout.LabelField("Grid Location neighbour", hex.GridInfo.GridLocation.ToString());
            }
        }

        riverWidth = EditorGUILayout.FloatField("River width:", riverWidth);
        riverGranularity = Mathf.Clamp01(EditorGUILayout.FloatField("River granularity", riverGranularity));
        riverDepth = EditorGUILayout.FloatField("River depth:", riverDepth);
        riverLength = EditorGUILayout.IntField("River length:", riverLength);

        if (GUILayout.Button("Calculate river from this hexagon"))
        {
            riverPaths = new List<List<Hexagon>>();
            CalculateRiverPaths((Hexagon)target, 2, riverLength);
            if(riverPaths.Count > 0)
            {
                selectedPath = riverPaths[UnityEngine.Random.Range(0, riverPaths.Count - 1)];
                meshInformation = new List<Vector3[]>();
                foreach(var hex in selectedPath)
                {
                    meshInformation.Add(hex.GetMesh().vertices);
                }
            }
        }

        if(GUILayout.Button("Calculate and show river information"))
        {
            CreateQuadraticBezierCurves(selectedPath);
            CreateRiverInformation();
        }

        if(bezierCurves != null && bezierCurves.Count > 0)
        {
            if (GUILayout.Button("Apply river"))
            {
                CreateRiverMesh();
                ApplyRiver();
            }
        }

        if(selectedPath != null)
        {
            if(GUILayout.Button("Save river path hexagons"))
            {
                foreach(var hex in selectedPath)
                {
                    hex.SaveToFile(FileManagementUtilities.GetDirectory());
                }
            }
        }
    }

    // Update is called once per frame
    private void OnEnable()
    {
    }
}
#endif