﻿using System;
using UnityEngine;

// Basic Camera controller script for testing
public class CameraController : MonoBehaviour {
    public Transform target;

    public MapLoader mapLoader;

    public float lookAtSpeed = 1f;

    private Vector3 previousTargetPosition;

	// Use this for initialization
	void Start () {
		
	}
    private void FixedUpdate()
    {
        Vector3 moveTo = (target.transform.position - transform.position) + Vector3.up * 5f;
        //float strength = (float)Math.Pow(moveTo.magnitude - 20, 1);
        transform.position = Vector3.Lerp(transform.position, transform.position + moveTo, Time.deltaTime);

        var targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, lookAtSpeed * Time.deltaTime);
    }

    // Update is called once per frame
    void LateUpdate () {
	    //Vector3 moveTo = Vector3.Lerp(transform.position, target.position + Vector3.up * 5, Time.deltaTime) - transform.position;
     //   moveTo = target.transform.position - transform.position + Vector3.up * 5f;
	    //float strength = (float)Math.Pow((target.transform.position - transform.position).magnitude - 20, 1);

	    //transform.Translate(moveTo * strength);
    }
}
