﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Class for wrapping environmental props with their survival rate
// This class is only used for the initial creation of props
// on the map. Most information will be stripped down to
// the EnvironmentalPropIdentifier only at runtime
[System.Serializable]
public class EnvironmentalPropIdentifierWrapper
{
    public EnvironmentalPropIdentifierWrapper(EnvironmentalPropIdentifier ident, float total, float competing)
    {
        identifier = ident;
        totalFitness = total;
        competingFitness = competing;
        placementSeed = ident.seed;
        placementDensity = ident.density;
    }
    public EnvironmentalPropIdentifier identifier;
    public float totalFitness;
    public float competingFitness;
    public float placementSeed;
    public float placementDensity;
}


//A manager managing multiple propFactories. Each prop has its own factory
// The job of the factory manager is to manage all these different factories and store them in a dictionary
// where they can be accessed with the prop they manage
[CreateAssetMenu(fileName = "New PrefabReferencingUtility", menuName = "PrefabManagement/PrefabReferencingUtility", order = 51)]
public class EnvironmentalObjectFactoryManager : ScriptableObject
{
    // big list of prefabs
    // for each prefab a factory will exist
    [SerializeField]
    public EnvironmentalPropIdentifier[] prefabs;

    // dictionary of factories, one for each prefab-type
    private Dictionary<PrefabType, EnvironmentalPropFactory> myPropFactories;

    // function for retrieving a prop from one of the factories
    public EnvironmentalProp GetPrefab(PrefabType type = PrefabType.RANDOM, PrefabSubType subType = PrefabSubType.RANDOM, int variationID = -1, EnvironmentalPropData data = null)
    {
        if(myPropFactories == null)
        {
            CreateFactories();
        }

        if(!myPropFactories.ContainsKey(type))
        {
            if(type == PrefabType.RANDOM)
            {
                type = prefabs[0].type;
            }
            else
            {
                // add the factory we need
                EnvironmentalPropIdentifier identifier = prefabs.FirstOrDefault(i => i.type == type);
                myPropFactories.Add(type, new EnvironmentalPropFactory(identifier));
            }
        }

        EnvironmentalPropFactory factory = myPropFactories[type];
        return factory.GetProp(variationID, subType, data);
    }

    #region Setup and Teardown
    private void CreateFactories()
    {
        myPropFactories = new Dictionary<PrefabType, EnvironmentalPropFactory>();
        foreach(var propIdentifier in prefabs)
        {
            if(!myPropFactories.ContainsKey(propIdentifier.type))
            {
                myPropFactories.Add(propIdentifier.type, new EnvironmentalPropFactory(propIdentifier));
            }
        }
    }
    public void DestroyAll()
    {
        if(myPropFactories != null)
        {
            foreach (var factory in myPropFactories)
            {
                factory.Value.ClearAll();
            }
            myPropFactories = null;
        }
    }
    #endregion
}