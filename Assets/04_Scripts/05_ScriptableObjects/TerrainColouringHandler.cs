﻿using System.Collections.Generic;
using UnityEngine;

// Struct defining a triangle which is then used
// to sample a certain area in the texture
// It is also used to generate a centre point
// inside the triangle to sample the colour at that point
public struct Triangle
{
    public Vector2 pointA { get; set; }
    public Vector2 pointB { get; set; }
    public Vector2 pointC { get; set; }

    public Vector2 pointCentre { get; set; }

    public void AddOffset(Vector2 offset)
    {
        pointA += offset;
        pointB += offset;
        pointC += offset;
        pointCentre += offset;
    }
    public void Initialise(bool input, int textureSize)
    {
        if (input)
        {
            pointA = new Vector2(1f/(textureSize*textureSize),2f/(textureSize*textureSize));
            pointB = new Vector2(1f / (textureSize * textureSize), 1f / (float)textureSize - 1f / (textureSize * textureSize));
            pointC = new Vector2(1f / (float)textureSize -2f / (textureSize*textureSize), 1f / (float)textureSize - 1f /(textureSize*textureSize));
        }
        else
        {
            pointA = new Vector2(2f / (textureSize * textureSize), 1f / (textureSize * textureSize));
            pointB = new Vector2(1f/ (float)textureSize - 1f/(textureSize*textureSize), 1f/(textureSize*textureSize));
            pointC = new Vector2(1f / (float)textureSize - 1f/(textureSize * textureSize), 1f / (float)textureSize - 2f/(textureSize*textureSize));
        }

        pointCentre = (pointA + pointB + pointC)/3f;
    }
}

// Class for colouring the hexagon
// If there are no shared vertices it will use a texture to paint the individual hexagon triangles
// If the vertices are shared it will use vertex colours to paint the hexagon

[ExecuteInEditMode]
[CreateAssetMenu(fileName = "New TerrainColouringHandler", menuName = "Environment/TerrainColouringHandler", order = 53)]
public class TerrainColouringHandler : ScriptableObject{

    #region Fields
    Vector2[] offsets;
    // Triangles one and two are the two triangles making up a square in the texture
    // Each texture consists of texturesize * texturesize squares
    // The texturesize can therefore be used to define how lare a square and therefore a triangle inside the square are
    Triangle one;
    Triangle two;
    Triangle[] triangles;
    Vector2[] uvs;

    public int TextureSize;

    public Texture2D texture;

    public static float gradient = 1.7320508075688f;
    private UnitVectors unitVectors;

    private bool vertexNorthOrientation = false;
    #endregion

    #region initialisation
    public void Initialise(bool vertexNorthOrientation = false)
    {
        this.vertexNorthOrientation = vertexNorthOrientation;
        one = new Triangle();
        two = new Triangle();
        triangles = new Triangle[TextureSize * TextureSize * 2];

        one.Initialise(true, TextureSize);
        two.Initialise(false, TextureSize);

        offsets = new Vector2[TextureSize * TextureSize];
        float stepSize = 1f / (float)TextureSize;

        for (int i = 0; i < TextureSize; i++)
        {
            for (int j = 0; j < TextureSize; j++)
            {
                offsets[i * TextureSize + j] = new Vector2(j * stepSize, i * stepSize);
            }
        }
        for (int i = 0; i < offsets.Length; i++)
        {
            Triangle replicaOne;
            Triangle replicaTwo;
            replicaOne = one;
            replicaOne.AddOffset(offsets[i]);
            replicaTwo = two;
            replicaTwo.AddOffset(offsets[i]);
            triangles[i * 2] = replicaOne;
            triangles[i * 2 + 1] = replicaTwo;
        }
    }
    #endregion

    #region Vertex colour calculations
    public void CalculateNoSharedVertTextureUVs(Hexagon hex)
    {
        Vector3[] vertices = hex.GetMesh().vertices;
        Vector2[] uvs = new Vector2[vertices.Length];

        CalculateTextureUVsType(vertices, uvs, hex);

        hex.GetMesh().uv = uvs;
    }

    public void CalculateVertexColours(Hexagon hex)
    {
        Vector3[] vertices = hex.GetMesh().vertices;

        // create new colors array where the colors will be created.
        Color[] colors = new Color[vertices.Length];

        for (int i = 0; i < vertices.Length; i++)
        {
            //get the source of colour for the current vertex
            Hexagon typeSource = WorldCreationRandomNumberSource.GetTypeSource(hex.GetNeighbours(), hex, vertices, i, vertexNorthOrientation);

            int variabilityFactor = Random.Range(0f, 1f) > 0.5f ? 0 : 1;
            int trianglesIndex = (int)typeSource.ElevationFoundationType * 16 + (int)typeSource.ClimateFoundationType * 2 + variabilityFactor;
            Vector2 centre = triangles[trianglesIndex].pointCentre;
            Color color = texture.GetPixelBilinear(centre.y, centre.x);
            colors[i] = color;
            //colors[i] = Color.Lerp(Color.black, Color.white, vertices[i].y / 20);
        }
        int[] hexTriangles = hex.GetMesh().triangles;
        Neighbours neighbours = hex.GetNeighbours();
        for(int i = 0; i < hexTriangles.Length; i += 3)
        {
            Vector3 a = vertices[hexTriangles[i+2]] - vertices[hexTriangles[i]];
            Vector3 b = vertices[hexTriangles[i+1]] - vertices[hexTriangles[i]];
            Vector3 c = vertices[hexTriangles[i + 2]] - vertices[hexTriangles[i + 1]];

            float angleA = Vector3.Angle(a, Vector3.up);
            float angleB = Vector3.Angle(b, Vector3.up);
            float angleC = Vector3.Angle(c, Vector3.up);

            bool angleASteep = angleA > 120 || angleA < 60;
            bool angleBSteep = angleB > 120 || angleB < 60;
            bool angleCSteep = angleC > 120 || angleC < 60;

            if(angleASteep || angleBSteep || angleCSteep)
            {
                Hexagon typeSource = WorldCreationRandomNumberSource.GetTypeSource(neighbours, hex, vertices, hexTriangles[i]);
                int trianglesIndex =  (int)typeSource.ElevationFoundationType * 16 + 10;
                Vector2 centre = triangles[trianglesIndex].pointCentre;
                Color color = texture.GetPixelBilinear(centre.x, centre.y);
                //color = Color.red;
                colors[hexTriangles[i + 2]] = color;
                colors[hexTriangles[i + 1]] = color;
                colors[hexTriangles[i]] = color;
            }
        }


        // assign the array of colors to the Mesh.
        hex.GetMesh().colors = colors;
    }
    public void CalculateTextureUVs(Mesh mesh, Transform position, int[] modifier)
    {
        // generate vectors important for texture mapping
        Vector3[] vertices = mesh.vertices;
        Vector2[] uvs = new Vector2[vertices.Length];

        // call calculating function
        CalculateTextureUVsAltitude(vertices ,uvs, modifier);
        mesh.uv = uvs;
    }

    #endregion

    #region Hexagon border colour correction
    // function corrects colours along hexagon borders
    public void CorrectHexagonColourBorders(IEnumerable<Hexagon> hexagons)
    {
        foreach (Hexagon hex in hexagons)
        {
            Vector3[] vertices = hex.GetMesh().vertices;
            Hexagon[] neighboursList = hex.GetNeighbours().GetArray();
            int neighboursIndex = 0;

            Color[] colors = hex.GetMesh().colors;
            for(int i = 0; i < hex.HexagonIterations * 6; i++)
            {
                if(i % hex.HexagonIterations == 0 && i != 0)
                {
                    neighboursIndex++;
                }
                if(neighboursIndex + 1 <= neighboursList.Length)
                {
                    // counts on which vertex of the current edge we are
                    int edgeVertexCount = i % hex.HexagonIterations;
                    // count of total vertices on the edge of hexagon
                    int totalEdgeVertexCount = hex.HexagonIterations * 6;

                    int mirroredIndex = (i + 4 * hex.HexagonIterations - 2 * edgeVertexCount) % totalEdgeVertexCount;
                    int completeMirroredIndex = hex.FirstEdgeVertexIndex + mirroredIndex;
                    int completeIndex = hex.FirstEdgeVertexIndex + i;
                    if (completeMirroredIndex + 1 <= neighboursList[neighboursIndex].GetMesh().colors.Length)
                    {
                        colors[completeIndex] = neighboursList[neighboursIndex].GetMesh().colors[completeMirroredIndex];
                    }
                        
                }
                
            }
            hex.GetMesh().colors = colors;
        }
    }

    Vector3 ReflectWithNormalVector(Vector3 toReflect, Vector3 normal)
    {
        return toReflect - (2 * Vector3.Dot(toReflect, normal) * normal);
    }
    #endregion

    #region UV Calculation for using texture
    private void CalculateTextureUVsType(Vector3[] vertices, Vector2[] uvs, Hexagon hex)
    {
        var neighbours = hex.GetNeighbours();

        for (int i = 0; i < vertices.Length; i += 3)
        {
            Hexagon typeSource = WorldCreationRandomNumberSource.GetTypeSource(neighbours, hex, vertices, i);
            //uvs[i] = triangles[(int)typeSource.GetElevationFoundationType() + 5].pointA;
            //uvs[i + 1] = triangles[(int)typeSource.GetElevationFoundationType() + 5].pointB;
            //uvs[i + 2] = triangles[(int)typeSource.GetElevationFoundationType() + 5].pointC;
            GenerateUVsForHexagon(typeSource, i, uvs, vertices);
        }
    }
    private void GenerateUVsForHexagon(Hexagon hex, int index, Vector2[] uvs, Vector3[] vertices)
    {
        int trianglesIndex;
        int elevationFoundationType = (int)hex.ElevationFoundationType;
        if (CheckAngleSteepness(index,vertices))
        {
            trianglesIndex = elevationFoundationType * 16 + 10;
        }
        else
        {
            int variabilityFactor = Random.Range(0f, 1f) > 0.5f ? 0 : 1;
            trianglesIndex = elevationFoundationType * 16 + (int)hex.ClimateFoundationType * 2 + variabilityFactor;
        }


        uvs[index] = triangles[trianglesIndex].pointA;
        uvs[index + 1] = triangles[trianglesIndex].pointB;
        uvs[index + 2] = triangles[trianglesIndex].pointC;
    }

    

    // will calculate UVs depending on gradient of the triangle
    public void CalculateTextureUVsGradient(Vector3[] vertices, Vector2[] uvs, int[] gradient)
    {
        for (int i = 0; i < vertices.Length; i += 3)
        {
            for (int j = 0; j < gradient.Length; j++)
            {
                float angleOne = Mathf.Abs(Vector3.Angle(vertices[i]-vertices[i+1], new Vector3(0,0,1)));
                float angleTwo = Mathf.Abs(Vector3.Angle(vertices[i + 1]- vertices[i+2], new Vector3(0,0,1)));
                float angleThree = Mathf.Abs(Vector3.Angle(vertices[i + 2] - vertices[i], new Vector3(0, 0, 1)));
                float betweenAngle = angleOne < angleTwo ? angleOne : angleTwo;
                float angle = betweenAngle < angleThree ? betweenAngle : angleThree;
                Debug.Log(angle);

                if (angle < gradient[j])
                {
                    uvs[i] = triangles[j + 4].pointA;
                    uvs[i + 1] = triangles[j + 4].pointB;
                    uvs[i + 2] = triangles[j + 4].pointC;
                    break;
                }

            }
        }
    }
    public void CalculateTextureUVsAltitude(Vector3[] vertices, Vector2[] uvs, int[] altitude)
    {
        for (int i = 0; i < vertices.Length; i += 3)
        {
            for (int j = 0; j < altitude.Length; j++)
            {
                if (vertices[i].y < altitude[j])
                {
                    uvs[i] = triangles[j + 4].pointA;
                    uvs[i + 1] = triangles[j + 4].pointB;
                    uvs[i + 2] = triangles[j + 4].pointC;
                    break;
                }
            }
        }
    }
    #endregion

    #region Utilities
    // function to check the steepness of a triangle on a non shared vertices 
    private static bool CheckAngleSteepness(int index, Vector3[] vertices)
    {
        // first check gradient of triangle
        Vector3 a = vertices[index + 2] - vertices[index];
        Vector3 b = vertices[index + 1] - vertices[index];
        Vector3 c = vertices[index + 2] - vertices[index + 1];

        float angleA = Vector3.Angle(a, Vector3.up);
        float angleB = Vector3.Angle(b, Vector3.up);
        float angleC = Vector3.Angle(c, Vector3.up);

        bool angleASteep = angleA > 120 || angleA < 60;
        bool angleBSteep = angleB > 120 || angleB < 60;
        bool angleCSteep = angleC > 120 || angleC < 60;

        return angleASteep || angleBSteep || angleCSteep;
    }
    #endregion
}
