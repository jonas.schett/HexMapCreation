﻿using UnityEngine;

// class describing the 6 neighbours of a given hexagon
// all hexagons will be a reference to the actual hexagon allowing for
// easier interaction with neighbours

// the neighbours are designed in a way to allow both sensible orientations of hexagons
// - a side facing north
// - a vertex facing north

// that is also the reason that there are a total of 8 neighbours and not the usual 6
public class Neighbours
{
    public Hexagon N { get { return HexGrid[(int)NV.x, (int)NV.y]; } }
    public Hexagon NE { get { return HexGrid[(int)NEV.x, (int)NEV.y]; } }
    public Hexagon E { get { return HexGrid[(int)EV.x, (int)EV.y]; } }
    public Hexagon SE { get { return HexGrid[(int)SEV.x, (int)SEV.y]; } }
    public Hexagon S { get { return HexGrid[(int)SV.x, (int)SV.y]; } }
    public Hexagon SW { get { return HexGrid[(int)SWV.x, (int)SWV.y]; } }
    public Hexagon W { get { return HexGrid[(int)WV.x, (int)WV.y]; } }
    public Hexagon NW { get { return HexGrid[(int)NWV.x, (int)NWV.y]; } }

    public Vector2 NV { get; private set; }
    public Vector2 NEV { get; private set; }
    public Vector2 EV { get; private set; }
    public Vector2 SEV { get; private set; }
    public Vector2 SV { get; private set; }
    public Vector2 SWV { get; private set; }
    public Vector2 WV { get; private set; }
    public Vector2 NWV { get; private set; }

    // array of all locations
    public Vector2[] locationArray { get; private set; }

    // Reference to the entire hexGrid
    public Hexagon[,] HexGrid { get; private set; }

    private bool vertexNorthOriented;

    public void InitialiseV(Vector2 gridLoc, int gridSize)
    {
        if(vertexNorthOriented)
        {
            NV = new Vector2((int)gridLoc.x, NumberConversions.mod((int)gridLoc.y + 1, gridSize));
            NEV = NV;
            EV = new Vector2(NumberConversions.mod((int)gridLoc.x + 1, gridSize), (int)gridLoc.y);
            SEV = new Vector2(NumberConversions.mod((int)gridLoc.x + 1, gridSize), NumberConversions.mod((int)gridLoc.y - 1, gridSize));
            SV = new Vector2((int)gridLoc.x, NumberConversions.mod((int)gridLoc.y - 1, gridSize));
            SWV = SV;
            WV = new Vector2(NumberConversions.mod((int)gridLoc.x - 1, gridSize), (int)gridLoc.y);
            NWV = new Vector2(NumberConversions.mod((int)gridLoc.x - 1, gridSize), NumberConversions.mod((int)gridLoc.y + 1, gridSize));

            locationArray = new Vector2[6] { NEV, EV, SEV, SWV, WV, NWV };
        }
        else
        {
            NV = new Vector2((int)gridLoc.x, NumberConversions.mod((int)gridLoc.y + 1, gridSize));
            NEV = new Vector2(NumberConversions.mod((int)gridLoc.x + 1, gridSize), NumberConversions.mod((int)gridLoc.y + 1, gridSize));
            EV = new Vector2(NumberConversions.mod((int)gridLoc.x + 1, gridSize), (int)gridLoc.y);
            SEV = EV;
            SV = new Vector2((int)gridLoc.x, NumberConversions.mod((int)gridLoc.y - 1, gridSize));
            SWV = new Vector2(NumberConversions.mod((int)gridLoc.x - 1, gridSize), NumberConversions.mod((int)gridLoc.y - 1, gridSize));
            WV = new Vector2(NumberConversions.mod((int)gridLoc.x - 1, gridSize), (int)gridLoc.y);
            NWV = WV;

            locationArray = new Vector2[6] { NEV, SEV, SV, SWV, NWV, NV };
        }
        
    }
    public Neighbours(Vector2 gridLoc, Hexagon[,] hexGrid, int gridSize, bool vertexNorthOriented)
    {
        this.vertexNorthOriented = vertexNorthOriented;
        HexGrid = hexGrid;
        InitialiseV(gridLoc, gridSize);
    }

    // an array that can be iterated over
    // the iteration will always start with the hexagon in the north east and go clockwise
    // don't ask why, this is simply what I did first
    public Hexagon[] GetArray()
    {
        Hexagon[] array = new Hexagon[6];
        if (!vertexNorthOriented)
        {
            array[0] = HexGrid[(int)NEV.x, (int)NEV.y];
            array[1] = HexGrid[(int)SEV.x, (int)SEV.y];
            array[2] = HexGrid[(int)SV.x, (int)SV.y];
            array[3] = HexGrid[(int)SWV.x, (int)SWV.y];
            array[4] = HexGrid[(int)NWV.x, (int)NWV.y];
            array[5] = HexGrid[(int)NV.x, (int)NV.y];
        }
        else
        {
            array[0] = HexGrid[(int)NEV.x, (int)NEV.y];
            array[1] = HexGrid[(int)EV.x, (int)EV.y];
            array[2] = HexGrid[(int)SEV.x, (int)SEV.y];
            array[3] = HexGrid[(int)SWV.x, (int)SWV.y];
            array[4] = HexGrid[(int)WV.x, (int)WV.y];
            array[5] = HexGrid[(int)NWV.x, (int)NWV.y];
        }
        return array;
    }
}
