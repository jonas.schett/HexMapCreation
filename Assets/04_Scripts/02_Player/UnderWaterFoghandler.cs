﻿using UnityEngine;

// Fog handler for under water scenes
// Ensures the fog under water gets denser and darker as well as blue shifted
public class UnderWaterFoghandler : MonoBehaviour
{
    private bool underWater;
    private Color fogColour;
    private Color underwaterFogColour;
    private float fogDensity;
    private float underwaterFogDensity;

    public float waterLevel = 0;
    // Start is called before the first frame update
    void Start()
    {
        fogColour = RenderSettings.fogColor;
        underwaterFogColour = fogColour;
        underwaterFogColour.r *= 0.2f;
        underwaterFogColour.g *= 0.5f;
        underwaterFogColour.b *= 0.8f;
        fogDensity = RenderSettings.fogDensity;
        underwaterFogDensity = fogDensity * 10f;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < waterLevel)
        {
            RenderSettings.fogColor = underwaterFogColour;
            RenderSettings.fogDensity = underwaterFogDensity;
        }
        else
        {
            RenderSettings.fogColor = fogColour;
            RenderSettings.fogDensity = fogDensity;
        }
    }
}
