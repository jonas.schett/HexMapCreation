﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class HexagonBuildingTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestHexagonEdgeVertexIndices()
        {
            // Use the Assert class to test conditions
            Assert.AreEqual(HexagonBuilder.GetEdgeVertexIndices(1), 6);
            Assert.AreEqual(HexagonBuilder.GetEdgeVertexIndices(2), 18);
            Assert.AreEqual(HexagonBuilder.GetEdgeVertexIndices(3), 36);
            Assert.AreEqual(HexagonBuilder.GetEdgeVertexIndices(4), 60);
            Assert.AreEqual(HexagonBuilder.GetEdgeVertexIndices(5), 90);

        }

        [Test]
        public void TestHexagonEdgeVertexIndicesRange()
        {
            // we want to ensure that the range of he vertices is correct
            int indexBegin;
            int indexEnd;
            HexagonBuilder.GetEdgeVertexIndexRange(out indexBegin, out indexEnd, 5);

            // the expected range for indexBegi
            Assert.AreEqual(indexBegin, 61);
            Assert.AreEqual(indexEnd, 90);

            HexagonBuilder.GetEdgeVertexIndexRange(out indexBegin, out indexEnd, 4);

            // the expected range for indexBegi
            Assert.AreEqual(indexBegin, 37);
            Assert.AreEqual(indexEnd, 60);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator HexagonBuildingTestsWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
