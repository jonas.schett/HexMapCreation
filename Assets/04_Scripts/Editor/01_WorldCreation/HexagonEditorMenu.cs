﻿#if (UNITY_EDITOR)

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

[ExecuteInEditMode]
// Hexagon editor window to modify indiviual hexagons after a map has been created
public class HexagonEditorMenu : HexagonEditor
{
    [MenuItem("Window/HexMapTools/HexEditor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(HexagonEditorMenu));
    }
    public void OnFocus()
    {

    }

    void OnSceneGUI()
    {

    }

    public override void OnGUI()
    {
        // Menu, has a number of buttons for interaction with the user
        base.OnGUI();
        if (hexagon)
        {
            int verticalLocation = 118;
            DrawHeader(ref verticalLocation, "Hexagon loading and saving");

            // Hexagon loading segment
            coordX = EditorGUI.IntField(new Rect(3, verticalLocation, position.width / 2 - 6, 20), "X", coordX);
            coordY = EditorGUI.IntField(new Rect(position.width / 2 + 3 , verticalLocation, position.width / 2 - 6, 20), "Y", coordY);

            deactivationDistance = EditorGUI.IntField(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Deactivation distance", deactivationDistance);

            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width / 2 - 6, 20), "Load Hexagon"))
            {
                Clear();
                LoadHexagon();
            }

            if (GUI.Button(new Rect(position.width / 2 + 3, verticalLocation, position.width / 2 - 6, 20), "Load Neighbours"))
            {
                ActivateNeighbours();
                DeactivateDistantHexagons();
            }

            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width / 2 - 6, 20), "Save changes"))
            {
                SaveAll();
            }


            if (GUI.Button(new Rect(position.width / 2 + 3, verticalLocation, position.width / 2 - 6, 20), "Unload Neighbours"))
            {
                UnloadNeighbours();
            }

            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Center selected hexagon"))
            {
                Hexagon selection = Selection.activeGameObject.GetComponent<Hexagon>();
                if (selection != null)
                {
                    ChangeMainHexagon(selection);
                    DeactivateDistantHexagons();
                }
            }

            if (GUI.Button(new Rect(3, verticalLocation+=23, position.width - 6, 20), "Clear all"))
            {
                Clear();
            }

            
            verticalLocation += 23;
            // next segment
            DrawHeader(ref verticalLocation, "Hexagon modification");

            if (GUI.Button(new Rect(3, verticalLocation, position.width / 2 - 6, 20), "Recalculate Hexagon vertices"))
            {
                RecalculateChangedHexagons();
            }
            if (GUI.Button(new Rect(position.width / 2 + 3, verticalLocation, position.width / 2 - 6, 20), "Recalculate Hexagon colours"))
            {
                RecalculateHexagons(new List<Hexagon> { mainHexagon }, false);
            }
            verticalLocation += 23;
            DrawHeader(ref verticalLocation, "Prop modification");

            propPrefabType = (PrefabType)EditorGUI.EnumPopup(new Rect(3, verticalLocation, position.width - 6, 20),"Prefab type", propPrefabType);
            propPrefabSubType = (PrefabSubType)EditorGUI.EnumPopup(new Rect(3, verticalLocation+= 20, position.width - 6, 20),"Prefab sub type", propPrefabSubType);
            propDensity = EditorGUI.FloatField(new Rect(3, verticalLocation+= 20, position.width - 6, 20),"Density (Props / ha)", propDensity);

            if (GUI.Button(new Rect(3, verticalLocation+=23, position.width / 3 - 3, 20), "Single"))
            {
                environmentalObjectPlacer.AddSinglePrefabToHexagons(Selection.gameObjects, propPrefabType, propPrefabSubType);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            if (GUI.Button(new Rect(position.width / 3 + 3, verticalLocation, position.width / 3 - 6, 20), "Single random (type)"))
            {
                environmentalObjectPlacer.AddSinglePrefabToHexagons(Selection.gameObjects, propPrefabType);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            if (GUI.Button(new Rect(2*position.width / 3 + 3, verticalLocation, position.width/3 - 6, 20), "Single random"))
            {
                environmentalObjectPlacer.AddSinglePrefabToHexagons(Selection.gameObjects);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }

            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width / 3 - 3, 20), "Multiple"))
            {
                environmentalObjectPlacer.AddPrefabsToMultipleHexagons(Selection.gameObjects, propDensity, propPrefabType, propPrefabSubType);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            if (GUI.Button(new Rect(position.width / 3 + 3, verticalLocation, position.width / 3 - 6, 20), "Multiple random (type)"))
            {
                environmentalObjectPlacer.AddPrefabsToMultipleHexagons(Selection.gameObjects, propDensity, propPrefabType);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            if (GUI.Button(new Rect(2 * position.width / 3 + 3, verticalLocation, position.width / 3 - 6, 20), "Multiple random"))
            {
                environmentalObjectPlacer.AddPrefabsToMultipleHexagons(Selection.gameObjects, propDensity);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width / 3 - 3, 20), "Multiple biased"))
            {
                environmentalObjectPlacer.AddMultipleBiasedPrefabsToHexagons(Selection.gameObjects, propDensity);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            if (GUI.Button(new Rect(position.width / 3 + 3, verticalLocation, position.width / 3 - 6, 20), "Details biased"))
            {
                environmentalObjectPlacer.AddBiasedDetailsToHexagon(Selection.gameObjects, propDensity * 5);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            if (GUI.Button(new Rect(2 * position.width / 3 + 3, verticalLocation, position.width / 3 - 6, 20), "Details"))
            {
                environmentalObjectPlacer.AddDetailsToMultipleHexagons(Selection.gameObjects, propDensity, propPrefabType, propPrefabSubType);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
            verticalLocation += 23;
            DrawHeader(ref verticalLocation, "River generation");
            if (GUI.Button(new Rect(2 * position.width / 3 + 3, verticalLocation, position.width / 3 - 6, 20), "Show river outline starting at hexagon"))
            {
                environmentalObjectPlacer.AddDetailsToMultipleHexagons(Selection.gameObjects, propDensity, propPrefabType, propPrefabSubType);
                AddHexGameobjectsToChangeTrackingList(Selection.gameObjects);
            }
        }
    }

    private int coordX;
    private int coordY;

    private int deactivationDistance;

    private Hexagon mainHexagon;

    // loads a single hexagon from the directory in which the files are stored
    // the load coordinates from coorX and coordY are used here
    private void LoadHexagon()
    {
        string directory = GetMapDirectory();
        string path = Path.Combine(directory, "mapInformation.hex");

        LoadMapInformation(path);

        if (hexagons == null)
        {
            hexagons = new List<Hexagon>();
        }

        if(coordY < gridSize && coordX < gridSize)
        {
            mainHexagon = hexGrid[coordX, coordY];
            ActivateHexagon(mainHexagon, new Vector3());
        }
        else
        {
            Debug.LogError("Cannot load hexagons outside the grid size");
        }
    }

    // method to clear the current hexagons (does not save)
    private void Clear()
    {
        if(hexagons != null)
        {
            foreach (var hex in hexagons)
            {
                if(hex!= null)
                {
                    DestroyImmediate(hex.gameObject, false);
                }
            }
            hexagons.Clear();
        }
        mainHexagon = null;
        environmentalObjectPlacer.GetEnvironmentalObjectReferenceUtility().DestroyAll();
        environmentalObjectPlacer.GetDetailObjectReferenceUtility().DestroyAll();
    }

    // method to save the main Hexagon to file
    private void SaveHexagon()
    {
        if(mainHexagon)
        {
            mainHexagon.SaveToFile(GetMapDirectory());
        }
    }

    // method to save all hexagons to file
    private void SaveAll()
    {
        foreach(Hexagon hex in changedHexagonsTrackingList)
        {
            if (hex != null)
            {
                hex.SaveToFile(GetMapDirectory());
            }
        }
        changedHexagonsTrackingList.Clear();
    }

    // method to change the main hexagon to the hexagon that is currently selected
    private void ChangeMainHexagon(Hexagon newMainHex)
    {
        //CheckForNullHexagons();
        Vector3 update = -newMainHex.transform.position;
        for (int i = 0; i < hexagons.Count; i++)
        {
            if(hexagons[i] != null)
            {
                hexagons[i].transform.Translate(update);
            }
        }
        mainHexagon = newMainHex;
        coordX = (int)mainHexagon.GridInfo.GridLocation.x;
        coordY = (int)mainHexagon.GridInfo.GridLocation.y;
        ActivateNeighbours(mainHexagon);
    }
    
    // method that deactivates hexagons if they are further away than the distance specified
    // in destroyDistance
    private void DeactivateDistantHexagons()
    {
        for(int i = 0; i < hexagons.Count; i ++)
        {
            if(hexagons[i].transform.position.magnitude > deactivationDistance)
            {
                hexagons[i].DeactivateInstant();
            }
        }
    }

    // method to activate the neighbours of the current hexagon so they can also be modified
    // Each time this is activated it will load neighbours of the currently active hexagons
    // It also offers support of loading the neighbours of a specified hexagon only
    private void ActivateNeighbours(Hexagon specificHexagon = null)
    {
        if(mainHexagon != null)
        {
            HashSet<Hexagon> currentlyActiveHexagons = new HashSet<Hexagon>();
            if (specificHexagon != null)
            {
                currentlyActiveHexagons.Add(specificHexagon);
            }
            else
            {
                foreach (Hexagon hex in hexGrid)
                {
                    if (hex.gameObject.activeInHierarchy)
                    {
                        currentlyActiveHexagons.Add(hex);
                    }
                }
            }
            
            foreach (Hexagon hex in currentlyActiveHexagons)
            {
                Hexagon[] neighbours = hex.GetNeighbours().GetArray();
                for (int i = 0; i < neighbours.Length; i++)
                {
                    if(!neighbours[i].gameObject.activeInHierarchy)
                    {
                        ActivateHexagon(neighbours[i], unitVectors.AsArray[i] + hex.transform.position);
                    }
                }
            }
        }
        else
        {
            Debug.LogError("Ensure that the main hexagon is loaded before loading neighbours in");
        }
    }

    private void UnloadNeighbours()
    {
        foreach(var hex in hexagons)
        {
            if(hex != mainHexagon && hex != null)
            {
                hex.DeactivateInstant();
            }
        }
    }

    // Function to activate individual hexagons
    private void ActivateHexagon(Hexagon hex, Vector3 offset = new Vector3())
    {
        // activateinstant instantly activates all components of the hexagon
        hex.ActivateInstant(environmentalObjectPlacer);
        hex.transform.position = offset;
    }
}
#endif