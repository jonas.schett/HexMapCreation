﻿using System;
using System.IO;
using LPWAsset;
using UnityEngine;

[Serializable]
[RequireComponent(typeof(LowPolyWaterScript))]
public class River : MonoBehaviour
{
    private LowPolyWaterScript water;
    private QuadraticBezier curve;
    private float width;

    #region Getters and Setters
    internal void SetData(Mesh mesh, QuadraticBezier curve, float width)
    {
        if(water == null)
        {
            water = GetComponent<LowPolyWaterScript>();
        }
        water.customMesh = mesh;
        this.width = width;
        this.curve = curve;
    }

    internal float GetDistance(Vector3 position)
    {
        if(curve != null)
        {
            float refF = 0f;
            return curve.GetTopDownDistanceFromCurve(position, 0.1f, ref refF);
        }
        return 0f;
    }
    internal bool CheckPointWithinRiver(Vector3 position)
    {
        if(curve != null)
        {
            float refF = 0f;
            float distance = curve.GetTopDownDistanceFromCurve(position, 0.1f, ref refF);
            if (distance < width)
            {
                return true;
            }
        }
        return false;
    }
    #endregion

    #region Loading and Saving
    internal void Load(BinaryReader reader)
    {
        width = reader.ReadSingle();
        int riverVertexLength = reader.ReadInt32();
        Vector3[] riverVertices = new Vector3[riverVertexLength];
        for (int i = 0; i < riverVertexLength; i++)
        {
            riverVertices[i] = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }
        int riverTrianglesLength = reader.ReadInt32();
        int[] riverTriangles = new int[riverTrianglesLength];
        for (int i = 0; i < riverTrianglesLength; i++)
        {
            riverTriangles[i] = reader.ReadInt32();
        }
        Mesh mesh = new Mesh();
        mesh.vertices = riverVertices;
        mesh.triangles = riverTriangles;
        mesh.RecalculateNormals();

        if (water == null)
        {
            water = GetComponent<LowPolyWaterScript>();
        }
        water.customMesh = mesh;
        water.enableReflection = false;

        // se if we have a curve attached
        if(reader.ReadBoolean())
        {
            Vector3 p0 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            Vector3 p1 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            Vector3 p2 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            curve = new QuadraticBezier(p0,p1,p2);
        }
    }

    internal void Save(BinaryWriter writer)
    {
        writer.Write(width);
        if(water == null)
        {
            water = GetComponent<LowPolyWaterScript>();
        }

        Mesh riverMesh = water.customMesh;
        if (riverMesh != null)
        {
            writer.Write(riverMesh.vertices.Length);
            foreach (var riverVertex in riverMesh.vertices)
            {
                writer.Write(riverVertex.x);
                writer.Write(riverVertex.y);
                writer.Write(riverVertex.z);
            }
            writer.Write(riverMesh.triangles.Length);
            foreach (var triangle in riverMesh.triangles)
            {
                writer.Write(triangle);
            }
        }

        if(curve != null)
        {
            writer.Write(true);
            writer.Write(curve.Point0.x);
            writer.Write(curve.Point0.y);
            writer.Write(curve.Point0.z);
            writer.Write(curve.Point1.x);
            writer.Write(curve.Point1.y);
            writer.Write(curve.Point1.z);
            writer.Write(curve.Point2.x);
            writer.Write(curve.Point2.y);
            writer.Write(curve.Point2.z);
        }
        else
        {
            // curve does not exist, we need to ensure the reader gets this
            writer.Write(false);
        }
    }

    #endregion
}
