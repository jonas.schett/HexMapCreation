﻿using UnityEngine;

// Static class for retrieving random numbers or noise
public static class WorldCreationRandomNumberSource
{
    public static float gradient = 1.7320508075688f;

    // function for gettinga type source for a given point on a hexagon, The type source may be the neighbour of the hexagon
    // and this is deliberately so, to allow for better border blending of plants, colours of vertices and props
    public static Hexagon GetTypeSource(Neighbours neighbours, Hexagon hex, Vector3[] vertices, int index, bool vertexNorthOrientation = false)
    {
        float distanceOne;
        float distanceTwo;
        if(!vertexNorthOrientation)
        {
            distanceOne = (Mathf.Abs(vertices[index].z) + gradient * Mathf.Abs(vertices[index].x)) / (2f * hex.GridInfo.InnerRadius);
            distanceTwo = Mathf.Abs(vertices[index].z) / hex.GridInfo.InnerRadius;
        }
        else
        {
            distanceOne = (Mathf.Abs(vertices[index].x) + gradient * Mathf.Abs(vertices[index].z)) / (2f * hex.GridInfo.InnerRadius);
            distanceTwo = Mathf.Abs(vertices[index].x) / hex.GridInfo.InnerRadius;
        }
        
        double vertexAngle;
        Hexagon neighbour;
        float distanceToUse;
        if (!vertexNorthOrientation)
        {
            vertexAngle = NumberConversions.Rad2Deg((double)Mathf.Atan2(vertices[index].z, vertices[index].x));
            if (vertexAngle <= -119.9f)
            {
                neighbour = neighbours.SW;
                distanceToUse = distanceOne;
            }
            else if (vertexAngle <= -59.9f)
            {
                neighbour = neighbours.S;
                distanceToUse = distanceTwo;
            }
            else if (vertexAngle <= 0.1f)
            {
                neighbour = neighbours.SE;
                distanceToUse = distanceOne;
            }
            else if (vertexAngle <= 60.1f)
            {
                neighbour = neighbours.NE;
                distanceToUse = distanceOne;
            }
            else if (vertexAngle <= 120.1f)
            {
                neighbour = neighbours.N;
                distanceToUse = distanceTwo;
            }
            else
            {
                neighbour = neighbours.NW;
                distanceToUse = distanceOne;
            }
        }
        else
        {
            vertexAngle = NumberConversions.Rad2Deg((double)Mathf.Atan2(-vertices[index].x, vertices[index].z));
            if (vertexAngle <= -119.9f)
            {
                neighbour = neighbours.SE;
                distanceToUse = distanceOne;
            }
            else if (vertexAngle <= -59.9f)
            {
                neighbour = neighbours.E;
                distanceToUse = distanceTwo;
            }
            else if (vertexAngle <= 0.1f)
            {
                neighbour = neighbours.NE;
                distanceToUse = distanceOne;
            }
            else if (vertexAngle <= 60.1f)
            {
                neighbour = neighbours.NW;
                distanceToUse = distanceOne;
            }
            else if (vertexAngle <= 120.1f)
            {
                neighbour = neighbours.W;
                distanceToUse = distanceTwo;
            }
            else
            {
                neighbour = neighbours.SW;
                distanceToUse = distanceOne;
            }
        }

        float randomNumber = Random.Range(0f, 1f);
        Hexagon typeSource;
        if (randomNumber > Mathf.Pow(distanceToUse, 4) / 2f)
        {
            typeSource = hex;
        }
        else
        {
            typeSource = neighbour;
        }
        return typeSource;
    }
}
