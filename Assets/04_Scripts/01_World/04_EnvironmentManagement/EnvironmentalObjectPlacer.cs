﻿using UnityEngine;

using System.Collections.Generic;
using Random = UnityEngine.Random;

// Class for placing props onto the map
// It acts as an interface between the prop factory manager and the hexagon editor menu
// The decisions of which props to place onto which type of hexagon is made here
[ExecuteInEditMode]
[CreateAssetMenu(fileName = "New EnvironmentalObjectPlacer", menuName = "Environment/EnvironmentalObjectPlacer", order = 52)]
public class EnvironmentalObjectPlacer : ScriptableObject
{
    // source for large props
    [SerializeField]
    public EnvironmentalObjectFactoryManager source;

    // source for detail props
    [SerializeField]
    public EnvironmentalObjectFactoryManager detailSource;

    // list of all the prefabs
    List<EnvironmentalPropIdentifierWrapper>[,] prefabPlacementInformation = null;
    List<EnvironmentalPropIdentifierWrapper>[,] grassPrefabPlacementInformation = null;

    #region Initialisation and Resets
    // Method resets the information about where to place props for a particular list of props
    // The ability of survival for individual props is also calculated here
    // THe ability of survival also depends on the ability of survival of other props in the same list
    private void ResetPrefabPlacementInformationForList(out List<EnvironmentalPropIdentifierWrapper>[,] list, EnvironmentalObjectFactoryManager manager)
    {
        list = new List<EnvironmentalPropIdentifierWrapper>[(int)HexagonClimateFoundationType.NUM_CL_FOUNDATIONTYPES, (int)HexagonElevantionFoundationType.NUM_EL_FOUNDATIONTYPES];

        foreach (EnvironmentalPropIdentifier identifier in manager.prefabs)
        {
            for (int i = 0; i < identifier.climateSurvivalTypes.Length; i++)
            {
                for (int j = 0; j < identifier.elevationSurvivalTypes.Length; j++)
                {
                    // calculate the indices for the climate and elevation foundation type
                    // we can also calculate the survivalRate of the particular prop in this combination
                    int climateFoundationTypeIndex = (int)identifier.climateSurvivalTypes[i];
                    int elevationFoundationTypeIndex = (int)identifier.elevationSurvivalTypes[j];
                    float survivalRate = identifier.climateSurvivalRates[i] * identifier.elevationSurvivalRates[j];

                    // reset this particular sublist
                    if (list[climateFoundationTypeIndex, elevationFoundationTypeIndex] == null)
                    {
                        list[climateFoundationTypeIndex, elevationFoundationTypeIndex] = new List<EnvironmentalPropIdentifierWrapper>();
                    }
                    list[climateFoundationTypeIndex, elevationFoundationTypeIndex].Add(new EnvironmentalPropIdentifierWrapper(identifier, survivalRate, survivalRate));
                }
            }
        }

        foreach (var placementInfo in list)
        {
            if (placementInfo != null)
            {
                float totalFitness = 0f;
                // generate the chances of competing against other objects here:
                foreach (var item in placementInfo)
                {
                    totalFitness += item.totalFitness;
                }
                for (int i = 0; i < placementInfo.Count; i++)
                {
                    placementInfo[i].competingFitness /= totalFitness;
                }
            }
        }
    }

    // resets the entire placement info
    private void ResetPrefabPlacementInformation()
    {
        ResetPrefabPlacementInformationForList(out prefabPlacementInformation, source);
        ResetPrefabPlacementInformationForList(out grassPrefabPlacementInformation, detailSource);
    }
    #endregion

    #region Adding props to hexagon
    // Method to add multiple objects to hexagon
    public void AddObjectsToHexagon(Hexagon hex, float density = 0.05f)
    {
        ResetPrefabPlacementInformation();
        // TODO: improve type selection
        PrefabType type = PrefabType.NONE;
        switch (hex.ElevationFoundationType)
        {
            case HexagonElevantionFoundationType.Coast:
                type = PrefabType.PalmTree01;
                break;
            case HexagonElevantionFoundationType.Midlands:
            case HexagonElevantionFoundationType.Highlands:
            case HexagonElevantionFoundationType.Lowlands:
                type = PrefabType.PineTree01;
                break;
        }
        if (type != PrefabType.NONE)
        {
            foreach (var position in GeneratePointsOnHexagon(hex, density, hex.gameObject.transform.position))
            {
                //decide which object to spawn
                //var prefabInstance = source.GetPrefab(type);
                var prefabInstance = source.GetPrefab();
                prefabInstance.transform.position = position;
                prefabInstance.transform.localRotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up);
                hex.AddChildObject(prefabInstance);
            }
        }
    }

    // Methods to add multiple objects to multiple hexagons
    public void AddPrefabsToMultipleHexagons(GameObject[] gameObjects,float density, PrefabType type = PrefabType.RANDOM, PrefabSubType subType = PrefabSubType.RANDOM)
    {
        foreach (var go in gameObjects)
        {
            Hexagon hex = go.GetComponent<Hexagon>();
            if(hex != null)
            {
                AddMultiplePrefabsToHexagon(hex, density, type, subType);
            }
        }
    }

    // Adding biased prefabs will look at the climate & elevation types of the hexagon
    public void AddMultipleBiasedPrefabsToHexagons(GameObject[] gameObjects, float density)
    {
        foreach (var go in gameObjects)
        {
            Hexagon hex = go.GetComponent<Hexagon>();
            if (hex != null)
            {
                
                AddMultipleBiasedPrefabsToHexagon(hex, density);
            }
        }
    }

    public void AddBiasedDetailsToHexagon(GameObject[] gameObjects, float density)
    {
        foreach (var go in gameObjects)
        {
            Hexagon hex = go.GetComponent<Hexagon>();
            if (hex != null)
            {
                AddBiasedDetailsToHexagon(hex, density);
            }
        }
    }

    public void AddDetailsToMultipleHexagons(GameObject[] gameObjects, float density, PrefabType type = PrefabType.RANDOM, PrefabSubType subType = PrefabSubType.RANDOM)
    {
        foreach (var go in gameObjects)
        {
            Hexagon hex = go.GetComponent<Hexagon>();
            if (hex != null)
            {
                AddDetailsToHexagon(hex, density, type, subType);
            }
        }
    }
    public void AddSinglePrefabToHexagons(GameObject[] gameObjects, PrefabType type = PrefabType.RANDOM, PrefabSubType subType = PrefabSubType.RANDOM)
    {
        foreach (var go in gameObjects)
        {
            Hexagon hex = go.GetComponent<Hexagon>();
            if (hex != null)
            {
                AddSinglePrefabToHexagon(hex, type, subType);
            }
        }
    }
    public void AddMultiplePrefabsToHexagon(Hexagon hex, float density, PrefabType type = PrefabType.RANDOM, PrefabSubType subType = PrefabSubType.RANDOM)
    {
        ResetPrefabPlacementInformation();
        float triangleDensity = GetTriangleDensityFromDensityPerHectare(hex, density);
        foreach (var position in GeneratePointsOnHexagon(hex, triangleDensity, Vector3.zero))
        {
            //decide which object to spawn
            //var prefabInstance = source.GetPrefab(type);
            var prefabInstance = source.GetPrefab(type, subType);
            prefabInstance.transform.position = position + hex.transform.position;
            prefabInstance.transform.localRotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up);
            hex.AddChildObject(prefabInstance);
        }
    }

    public void AddBiasedDetailsToHexagon(Hexagon hex, float density)
    {
        ResetPrefabPlacementInformation();
        float triangleDensity = GetTriangleDensityFromDensityPerHectare(hex, density);
        int totalGrassAdded = 0;
        for(var i = 0; i < triangleDensity; i ++)
        {
            foreach (var prefabIdentifier in GenerateBiasedPointsOnHexagon(hex, triangleDensity, Vector3.zero, grassPrefabPlacementInformation))
            {

                //decide which object to spawn
                //var prefabInstance = source.GetPrefab(type);
                var prefabInstance = detailSource.GetPrefab(prefabIdentifier.Value.type);
                prefabInstance.transform.position = prefabIdentifier.Key + hex.transform.position;
                prefabInstance.transform.localRotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up);
                hex.AddGrass(prefabInstance);
                totalGrassAdded++;
            }
        }
        Debug.Log("Total grass added: " + totalGrassAdded);
    }

    public void AddDetailsToHexagon(Hexagon hex, float density, PrefabType type = PrefabType.RANDOM, PrefabSubType subType = PrefabSubType.RANDOM)
    {
        ResetPrefabPlacementInformation();
        float triangleDensity = GetTriangleDensityFromDensityPerHectare(hex, density);
        for (var i = 0; i < triangleDensity; i++)
        {
            foreach (var prefabIdentifier in GeneratePointsOnHexagon(hex, triangleDensity, Vector3.zero))
            {

                //decide which object to spawn
                //var prefabInstance = source.GetPrefab(type);
                var prefabInstance = detailSource.GetPrefab(type, subType);
                prefabInstance.transform.position = prefabIdentifier + hex.transform.position;
                prefabInstance.transform.localRotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up);
                hex.AddGrass(prefabInstance);
            }
        }
    }

    public void AddMultipleBiasedPrefabsToHexagon(Hexagon hex, float density)
    {
        ResetPrefabPlacementInformation();
        float triangleDensity = GetTriangleDensityFromDensityPerHectare(hex, density);
        foreach (var prefabIdentifier in GenerateBiasedPointsOnHexagon(hex, triangleDensity, Vector3.zero, prefabPlacementInformation))
        {

            //decide which object to spawn
            //var prefabInstance = source.GetPrefab(type);
            var prefabInstance = source.GetPrefab(prefabIdentifier.Value.type);
            prefabInstance.transform.position = prefabIdentifier.Key + hex.transform.position;
            prefabInstance.transform.localRotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up);
            hex.AddChildObject(prefabInstance);
        }
    }

    public void AddSinglePrefabToHexagon(Hexagon hex, PrefabType type = PrefabType.RANDOM, PrefabSubType subType = PrefabSubType.RANDOM)
    {
        ResetPrefabPlacementInformation();
        if (hex != null)
        {
            Vector3 position = hex.GetMesh().vertices[0];
            var prefabInstance = source.GetPrefab(type, subType);
            prefabInstance.transform.position = position + hex.transform.position;
            prefabInstance.transform.localRotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up);
            hex.AddChildObject(prefabInstance);
        }
        else
        {
            Debug.LogError("Ensure you have a hexagon loaded before adding prefabs to it");
        }
    }
    #endregion

    #region Prop position calculations
    // Method to generate points on the hexagon on which the props can be placed
    public List<Vector3> GeneratePointsOnHexagon(Hexagon hex, float density, Vector3 meshOffset)
    {
        Mesh mesh = hex.GetMesh();
        List<Vector3> locationList = new List<Vector3>();
        for(int i = 0; i < mesh.triangles.Length; i += 3)
        {
            //Vector3 trianglePosition = (mesh.vertices[mesh.triangles[i]] + mesh.vertices[mesh.triangles[i + 1]] + mesh.vertices[mesh.triangles[i + 2]]) /3f;
            //trianglePosition += hex.GridInfo.AbsoluteLocation;
            //float test = Mathf.PerlinNoise(trianglePosition.x * 0.05f, trianglePosition.z * 0.05f);

            
            float test = Random.Range(0f, 1f);
            if(test < density)
            {
                Vector3 point = CreatePointOnTriangle(mesh.vertices, mesh.triangles, i) + meshOffset;
                if(hex.HasRiver())
                {
                    if (!hex.GetRiver().CheckPointWithinRiver(point))
                    {
                        locationList.Add(point);
                    }
                }
                else
                {
                    locationList.Add(point);
                }
                
            }
        }
        return locationList;
    }


    private Vector3 CreatePointOnTriangle(Vector3[] vertices, int[] triangles, int triangleIndex)
    {
        Vector3 a = vertices[triangles[triangleIndex]];
        Vector3 b = vertices[triangles[triangleIndex + 1]];
        Vector3 c = vertices[triangles[triangleIndex + 2]];

        //generate point on triangle
        Vector3 ab = b - a;
        Vector3 ac = c - a;

        // get weights for generating the point on the triangle

        float randomNumber = Random.Range(0f, 1f);
        float randomSeparator = Random.Range(0f, 1f);

        float w1 = randomNumber * randomSeparator;
        float w2 = randomNumber * (1 - randomSeparator);

        // return a point that is on the triangle
        // if the sum of the weigts remains between 0 and 1 the point will be on the triangle
        return a + ab * w1 + ac * w2;
    }

    private float GetTriangleDensityFromDensityPerHectare(Hexagon hex, float density)
    {
        float areaOfHexagon = hex.GridInfo.OuterRadius * hex.GridInfo.OuterRadius * (3 * Mathf.Sqrt(3) / 2f);
        int numberOfTrianglesOnHexagon = hex.GetMesh().triangles.Length / 3;
        float areaPerTriangle = areaOfHexagon / numberOfTrianglesOnHexagon;
        return density * areaPerTriangle * 0.0001f;
    }


    // this function generates points on the hexagon for placement of specific prefabs, depending on their survivalrate and the makeup of the hexagon itself
    // It will depend on steepness of slope, climate, elevation, other competing props & luck
    public Dictionary<Vector3, EnvironmentalPropIdentifier> GenerateBiasedPointsOnHexagon(Hexagon hex, float density, Vector3 meshOffset, List<EnvironmentalPropIdentifierWrapper>[,] EnvironmentalPropPlacementInformation)
    {
        Mesh mesh = hex.GetMesh();
        Dictionary<Vector3, EnvironmentalPropIdentifier> locationList = new Dictionary<Vector3, EnvironmentalPropIdentifier>();
        List<EnvironmentalPropIdentifierWrapper> relevantList;

        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            // get the relevant source for placing objects
            Hexagon source = WorldCreationRandomNumberSource.GetTypeSource(hex.GetNeighbours(), hex, mesh.vertices, mesh.triangles[i], hex.GridInfo.VertexNorthOriented);
            // get the relevant list from our source
            relevantList = EnvironmentalPropPlacementInformation[(int)source.ClimateFoundationType, (int)source.ElevationFoundationType];
            if(relevantList == null)
            {
                continue;
            }
            // select one of the relevant props from the list using a fitness function
            EnvironmentalPropIdentifierWrapper wrapper = SelectRoulette(relevantList);

            // decide whether or not to place a prop to this place
            Vector3 trianglePosition = (mesh.vertices[mesh.triangles[i]] + mesh.vertices[mesh.triangles[i + 1]] + mesh.vertices[mesh.triangles[i + 2]]) /3f;
            trianglePosition += hex.GridInfo.AbsoluteLocation;
            float test = NoiseGenerator.GenerateNoiseForPropPlacement(trianglePosition, wrapper.placementDensity, wrapper.placementSeed, 1);

            if (test < density * wrapper.totalFitness)
            {
                Vector3 point = CreatePointOnTriangle(mesh.vertices, mesh.triangles, i) + meshOffset;
                if (hex.HasRiver())
                {
                    if (!hex.GetRiver().CheckPointWithinRiver(point))
                    {
                        locationList.Add(point, wrapper.identifier);
                    }
                }
                else
                {
                    locationList.Add(point, wrapper.identifier);
                }
            }
        }
        return locationList;
    }

    // a function to select a prop from a list of props. The selection is biased towards the strongest prop
    public static EnvironmentalPropIdentifierWrapper SelectRoulette(List<EnvironmentalPropIdentifierWrapper> environmentalProps)
    {
        float total = 0;
        float amount = Random.Range(0f, 1f);
        foreach(var item in environmentalProps)
        {
            if(amount < total + item.competingFitness)
            {
                return item;
            }
            total += item.competingFitness;
        }
        return null;
    }
    #endregion

    #region Getters
    public EnvironmentalObjectFactoryManager GetEnvironmentalObjectReferenceUtility()
    {
        return source;
    }

    public EnvironmentalObjectFactoryManager GetDetailObjectReferenceUtility()
    {
        return detailSource;
    }
    #endregion
}