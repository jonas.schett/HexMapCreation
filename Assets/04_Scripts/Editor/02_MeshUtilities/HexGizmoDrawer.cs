﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexGizmoDrawer : MonoBehaviour {

    public List<Vector3> vertices;
	// Use this for initialization
	void Start () {
        vertices = new List<Vector3>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        foreach(var vertex in vertices)
        {
            Gizmos.DrawCube(vertex, Vector3.one*2);
        }
    }
}
