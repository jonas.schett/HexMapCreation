﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

// Class responsible for loading the map at runtime
// It has a number of loadpoints which are positions
// into which a hexagon will be loaded in case the player coimes close enough
// the reference to the player can be assigned in the unioty editor

// There is also a load/destroydistance. They decide at which distances from the player the hexagons
// will become disabled/enabled

// The mapLoader will also move the entire map back to the origin, if the player moves far enough to avoid
// floating point accuracy problems
public class MapLoader : MonoBehaviour
{
    #region Fields
    [Header("Environmental object source")]
    [SerializeField]
    private EnvironmentalObjectFactoryManager environmentalReferenceUtility = null;
    [SerializeField]
    private EnvironmentalObjectFactoryManager detailReferenceUtility = null;
    [Header("Player and Hexagon prefab")]
    [SerializeField]
    private Transform player = null;
    [SerializeField]
    private Hexagon hexagon = null;

    [Header("Destroy and Load settings")]
    [SerializeField]
    private int destroyDistance = 1100;
    [SerializeField]
    private int loadDistance = 1000;

    private Hexagon[,] hexGrid;

    private float outerRadius;
    private float innerRadius;

    private UnitVectors unitVectors;
    private int gridSize;
    private Hexagon center;
    private Dictionary<Vector2, Hexagon> hexes;
    private Dictionary<Vector2, Vector3> toLoad;
    private List<Vector2> toDestroy;
    private Dictionary<Vector2, Vector3> activationPoints;

    private bool vertexNorthOrientation = false;
    #endregion

    #region Initialisation
    void Start ()
	{
	    Init();
        loadHexagon(new Vector2(0,0), new Vector3());
	}

    // INitialises all values needed.
    void Init()
    {
        // load information file
        string directory = GetDirectory();
        string path = Path.Combine(directory, "mapInformation.hex");
        BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open));
        gridSize = reader.ReadInt32();
        outerRadius = reader.ReadSingle();
        vertexNorthOrientation = reader.ReadBoolean();
        hexGrid = new Hexagon[gridSize, gridSize];
        reader.Close();

        innerRadius = Mathf.Sqrt(outerRadius * outerRadius - (outerRadius / 2f) * (outerRadius / 2f));
        unitVectors.Initialise(outerRadius, innerRadius,vertexNorthOrientation);
        hexes = new Dictionary<Vector2, Hexagon>();
        toLoad = new Dictionary<Vector2, Vector3>();
        activationPoints = new Dictionary<Vector2, Vector3>();
        toDestroy = new List<Vector2>();

        for (int i = 0; i < gridSize; i ++)
        {
            for(int j = 0; j < gridSize; j ++)
            {
                GridInformation gridInfo = new GridInformation(gridSize, new Vector2(i, j), outerRadius, vertexNorthOrientation);
                // initialise hexagon data for later loading
                hexGrid[i, j] = Instantiate(hexagon);
                hexGrid[i, j].GiveData(hexGrid, gridInfo);
                hexGrid[i, j].Deactivate();
                hexGrid[i, j].LoadFromFile(gridInfo, directory);
                hexGrid[i, j].transform.SetParent(transform);
                hexes.Add(gridInfo.GridLocation, hexGrid[i, j]);
            }
        }

        // turn on fog
        RenderSettings.fog = true;
    }
    #endregion

    #region Loading and destroying tiles
    void loadHexagon(Vector2 gridLocation, Vector3 offset)
    {
        Hexagon hex = hexGrid[(int)gridLocation.x, (int)gridLocation.y];
        //TODO change to relative position of hexagon
        hex.transform.position = transform.position + offset;

        hex.Activate(environmentalReferenceUtility);
    }
	void Update () {
        
	    Vector3 location = player.localPosition;

        // destroy hexagons
        // we also add destroyed hexagons to the loadPoints, as we may have to load them again
        foreach (var hex in hexes.Where(h => h.Value.Active))
        {
            if ((hex.Value.transform.position - location).magnitude > destroyDistance)
            {
                if (!activationPoints.ContainsKey(hex.Value.GetGridLocation()))
                {
                    activationPoints.Add(hex.Value.GetGridLocation(), hex.Value.transform.position);
                }
                //Debug.Log("destroying Hexagon: " + hex.Key);
                toDestroy.Add(hex.Key);
            }
        }

        // and finally we destroy hexagons
        if (toDestroy.Count > 0)
        {
            destroyHexagons();
        }

        // load hexagons
        foreach (KeyValuePair<Vector2, Vector3> loadPoint in activationPoints)
        {
            if ((loadPoint.Value - location).magnitude < loadDistance)
            {
                if(!toLoad.ContainsKey(loadPoint.Key))
                {
                    toLoad.Add(loadPoint.Key, loadPoint.Value);
                }
            }
        }

        // then we remove the toLoad list from the loadpoints
        foreach(KeyValuePair<Vector2, Vector3> loadPoint in toLoad)
        {
            if(activationPoints.ContainsKey(loadPoint.Key))
            {
                activationPoints.Remove(loadPoint.Key);
            }
        }
	    
        // then we actually load the hexagons from the toLoad list
        if(toLoad.Count > 0)
        {
            loadHexagons();
        }

        // check if we need to activate the grass
        // update activation points
        foreach (var hex in hexes.Where(h => h.Value.Active))
        {
            if (Vector3.Distance(hex.Value.transform.position, location) < loadDistance * 0.5f)
            {
                hex.Value.StartDetailActivation(detailReferenceUtility);
            }
            else if (Vector3.Distance(hex.Value.transform.position, location) > destroyDistance * 0.5f)
            {
                hex.Value.StartDetailDeactivation();
            }
        }

        // update activation points
        foreach (var hex in hexes.Where(h =>h.Value.Active))
        {
            //loadpoints for loading new hexagons
            setActivationPoints(hex.Value, hex.Value.transform.position);
        }

        if (new Vector2(location.x, location.z).magnitude > outerRadius * 1.1)
        {
            double vertexAngle = NumberConversions.Rad2Deg(Mathf.Atan2(player.transform.position.z, player.transform.position.x));
            if (vertexAngle <= -120.1f)
            {
                moveChildren(unitVectors.FromSouthWest);
            }
            else if (vertexAngle <= -60.1f)
            {
                moveChildren(unitVectors.FromSouth);
            }
            else if (vertexAngle <= 0f)
            {
                moveChildren(unitVectors.FromSouthEast);
            }
            else if (vertexAngle <= 60f)
            {
                moveChildren(unitVectors.FromNorthEast);
            }
            else if (vertexAngle <= 120f)
            {
                moveChildren(unitVectors.FromNorth);
            }
            else
            {
                moveChildren(unitVectors.FromNorthWest);
            }
        }
    }
    
    private void setActivationPoints(Hexagon hex, Vector3 offset)
    {
        Hexagon[] neighbours = hex.GetNeighbours().GetArray();
        Vector2[] neighbourLocations = hex.GetNeighbours().locationArray;

        for (int i = 0; i < neighbours.Length || i < neighbourLocations.Length; i++)
        {
            if (!neighbours[i].Active)
            {
                if (!activationPoints.ContainsKey(neighbourLocations[i]))
                {
                    activationPoints.Add(neighbourLocations[i], unitVectors.AsArray[i] + offset);
                }
                else
                {
                    activationPoints[neighbourLocations[i]] = unitVectors.AsArray[i] + offset;
                }
            }
        }
    }

    void moveChildren(Vector3 vector)
    {
        foreach (Hexagon child in this.GetComponentsInChildren<Hexagon>())
        {
            child.transform.position += vector; 
        }
        List<Vector2> keys = new List<Vector2>(activationPoints.Keys);
        foreach(var key in keys)
        {
            if(activationPoints.ContainsKey(key))
            {
                activationPoints[key] += vector;
            }
        }

        player.transform.position += vector;
    }

    void destroyHexagons()
    {
        //Debug.Log("destroying "+toDestroy.Count + " hexagons");
        foreach (var hex in toDestroy)
        {
            if(!activationPoints.ContainsKey(hex))
            {
                activationPoints.Add(hex, hexes[hex].transform.position);
            }
            hexes[hex].Deactivate();
        }
        toDestroy.Clear();
    }
    void loadHexagons()
    {
        //Debug.Log("loading " +toLoad.Count+" hexagons");
        foreach (KeyValuePair<Vector2, Vector3> var in toLoad)
        {
            if (hexes.ContainsKey(var.Key))
            {
                loadHexagon(var.Key, var.Value);
            }
        }
        toLoad.Clear();
    }
    #endregion

    #region Utilities
    private string GetDirectory()
    {
        string directory = Path.Combine(Application.persistentDataPath, "MapData");
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }
        return directory;
    }
    #if (UNITY_EDITOR)
    private void OnDrawGizmos()
    { 
        if(activationPoints != null)
        {
            Gizmos.color = Color.green;
            foreach (var loadPoint in activationPoints)
            {
                Gizmos.DrawSphere(loadPoint.Value + new Vector3(0, 20, 0), 20);
            }
        }
        
    }
    #endif
    #endregion
}
