﻿using UnityEngine;

public static class MUtils
{
    public static bool CompareFloats(float a, float b, float compareDelta)
    {
        if (Mathf.Abs(a - b) < compareDelta)
        {
            return true;
        }
        return false;
    }
}
