﻿#if (UNITY_EDITOR)

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class HexagonEditor : EditorWindow
{
    public EnvironmentalObjectPlacer environmentalObjectPlacer;
    
    public Hexagon hexagon;
    public int gridSize = 10;
    public float outerRadius;
    public float innerRadius; 

    protected UnitVectors unitVectors;
    public bool usesSharedVertices = false;
    public TerrainColouringHandler terrainColouringHandler;
    public NoiseGenerator noiseGenerator;
    protected List<Hexagon> hexagons;
    protected Dictionary<Hexagon, Vector3[]> hexagonVertices;
    protected Hexagon[,] hexGrid;
    protected HexagonData[,] trackingHexagonData;

    protected float propDensity;
    protected PrefabType propPrefabType;
    protected PrefabSubType propPrefabSubType;

    protected List<Task> noiseGenerationTasks = new List<Task>();

    protected HashSet<Hexagon> changedHexagonsTrackingList = new HashSet<Hexagon>();

    public bool vertexNorthOrientation = false;

    public virtual void OnGUI()
    {
        hexagon = (Hexagon)EditorGUI.ObjectField(new Rect(3, 3, position.width - 6, 20),"Hexagon prefab", hexagon, typeof(Hexagon), false);
        environmentalObjectPlacer = EditorGUI.ObjectField(new Rect(3, 26, position.width - 6, 20),"Prop placer", environmentalObjectPlacer, typeof(EnvironmentalObjectPlacer), false) as EnvironmentalObjectPlacer;
        noiseGenerator = EditorGUI.ObjectField(new Rect(3, 49, position.width - 6, 20),"Noise generator", noiseGenerator, typeof(NoiseGenerator), false) as NoiseGenerator;
        terrainColouringHandler = EditorGUI.ObjectField(new Rect(3, 72, position.width - 6, 20),"Terrain colouring handler", terrainColouringHandler, typeof(TerrainColouringHandler), false) as TerrainColouringHandler;
        usesSharedVertices = EditorGUI.Toggle(new Rect(3, 95, position.width - 6, 20), "Shared Vertices", usesSharedVertices);
    }

    public static string GetMapDirectory()
    {
        string directory = Path.Combine(Application.persistentDataPath, "MapData");
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }
        return directory;
    }

    protected void LoadMapInformation(string path)
    {
        BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open));

        gridSize = reader.ReadInt32();
        outerRadius = reader.ReadSingle();
        vertexNorthOrientation = reader.ReadBoolean();
        reader.Close();

        CreateHexagonLists();

        innerRadius = Mathf.Sqrt(outerRadius * outerRadius - (outerRadius / 2f) * (outerRadius / 2f));
        unitVectors.Initialise(outerRadius, innerRadius, vertexNorthOrientation);

        // load all hexagons
        LoadAllHexagons();
    }

    // function that creates all relevant lists for storing hexagon data
    protected void CreateHexagonLists()
    {
        hexGrid = new Hexagon[gridSize, gridSize];
        trackingHexagonData = new HexagonData[gridSize, gridSize];

        if (hexagons == null)
        {
            hexagons = new List<Hexagon>();
        }
        hexagons.Clear();

        if (hexagonVertices == null)
        {
            hexagonVertices = new Dictionary<Hexagon, Vector3[]>();
        }
        hexagonVertices.Clear();
    }

    // function to load all hexagons, but not activate them.
    protected void LoadAllHexagons()
    {
        for(int i = 0; i < gridSize; i ++)
        {
            for(int j = 0; j < gridSize; j ++)
            {
                GridInformation gridInfo = new GridInformation(gridSize, new Vector2(i, j), outerRadius, vertexNorthOrientation);
                hexGrid[i, j] = Instantiate(hexagon);
                hexGrid[i, j].GiveData(hexGrid, gridInfo);
                hexGrid[i, j].DeactivateInstant();
                hexGrid[i, j].LoadFromFile(gridInfo, GetMapDirectory());
                hexGrid[i, j].Recalculate();
                hexagons.Add(hexGrid[i, j]);
                trackingHexagonData[i,j] = hexGrid[i, j].GenerateHexagonData();
            }
        }
    }

    protected void DrawHeader(ref int location, string Text)
    {
        EditorGUI.DrawRect(new Rect(3, location, position.width - 6, 20), Color.gray);
        EditorGUI.LabelField(new Rect(3, location, position.width - 6, 20), Text);
        location += 23;
    }

    protected void AddHexGameobjectsToChangeTrackingList(GameObject[] objects)
    {
        foreach(var obj in objects)
        {
            Hexagon hex = obj.GetComponent<Hexagon>();
            if(hex != null)
            {
                changedHexagonsTrackingList.Add(hex);
            }
        }
    }

    protected void StartNoiseGeneration(NoiseGenerator noiseGen, Vector3[] vertices, Hexagon hex)
    {
        if(hexagonVertices.ContainsKey(hex))
        {
            hexagonVertices[hex] = noiseGen.AddNoiseToHexagon(hex, vertices);
        }
        else
        {
            hexagonVertices.Add(hex, noiseGen.AddNoiseToHexagon(hex, vertices));
        }
    }

    protected void RecalculateHexagons(IEnumerable<Hexagon> hexagons, bool applyVertices)
    {
        terrainColouringHandler.Initialise(vertexNorthOrientation);
        foreach (Hexagon hex in hexagons)
        {
            hex.AboveSeaLevel = true;
            if(applyVertices)
            {
                if(hexagonVertices.ContainsKey(hex))
                {
                    hex.ApplyVertices(hexagonVertices[hex]);
                }
                else
                {
                    Debug.LogError("Vertices used for calculation were empty");
                }
            }

            if(!usesSharedVertices)
            {
                terrainColouringHandler.CalculateNoSharedVertTextureUVs(hex);
            }
            else
            {
                terrainColouringHandler.CalculateVertexColours(hex);
            }
            hex.Recalculate();
            hex.AddCollider();

            foreach(var vertex in hex.GetMesh().vertices)
            {
                if(vertex.y < 0)
                {
                    hex.AboveSeaLevel = false;
                }
            }

            hex.SetSeaActive(!hex.AboveSeaLevel);
            changedHexagonsTrackingList.Add(hex);
        }
        // if we used shared vertices, we want to ensure that the vertices at the edge of hexagons have the same colour
        if(usesSharedVertices)
        {
            terrainColouringHandler.CorrectHexagonColourBorders(hexagons);
        }
        
    }

    protected void RecalculateChangedHexagons()
    {
        HashSet<Hexagon> changedHexagons = new HashSet<Hexagon>();
        foreach (var hex in hexGrid)
        {
            HexagonData trackingHexagon = trackingHexagonData[(int)hex.GridInfo.GridLocation.x, (int)hex.GridInfo.GridLocation.y];

            bool climateFoundationTypeChanged = hex.ClimateFoundationType != trackingHexagon.ClimateFoundationType;
            bool elevationFoundationTypeChanged = hex.ElevationFoundationType != trackingHexagon.ElevationFoundationType;
            bool landscapeFoundationTypeChanged = hex.LandscapeFoundationType != trackingHexagon.LandscapeFoundationType;

            if (climateFoundationTypeChanged || elevationFoundationTypeChanged || landscapeFoundationTypeChanged)
            {
                changedHexagons.Add(hex);
                foreach (Hexagon changed in hex.GetNeighbours().GetArray())
                {
                    changedHexagons.Add(changed);
                }
            }
        }

        Mesh[] hexagonMeshes = new Mesh[changedHexagons.Count];
        int counter = 0;
        foreach (var hex in changedHexagons)
        {
            NoiseGenerator ng = CreateInstance("NoiseGenerator") as NoiseGenerator;
            ng.TransferData(noiseGenerator, vertexNorthOrientation);

            // ensure we use reset vertices to begin with
            Vector3[] vertices = hex.GetVertices();
            for (var i = 0; i < vertices.Length; i++)
            {
                vertices[i].y = 0;
            }
            noiseGenerationTasks.Add(Task.Factory.StartNew(() => StartNoiseGeneration(ng, vertices, hex)));
            changedHexagonsTrackingList.Add(hex);
            hexagonMeshes[counter++] = hex.GetMesh();
        }

        // ensure all tasks are completed before we move on
        foreach (var task in noiseGenerationTasks)
        {
            Task.WaitAll(task);
        }
        Undo.RecordObjects(hexagonMeshes, "Meshes changed");
        RecalculateHexagons(changedHexagons, true);

        foreach (var hex in changedHexagons)
        {
            // apply changes to our tracking information
            trackingHexagonData[(int)hex.GridInfo.GridLocation.x, (int)hex.GridInfo.GridLocation.y] = hex.GenerateHexagonData();
        }

        //clear the map to ensure nothing gets modified multiple times
        changedHexagons.Clear();
    }
}

#endif
