﻿// enum storing elevation foundation types
public enum HexagonElevantionFoundationType
{
    DeepSea = 0,
    Sea,
    Coast,
    Lowlands,
    Midlands,
    Highlands,
    Mountain,
    HighMountain,
    // this below item is always last
    NUM_EL_FOUNDATIONTYPES
}

// enum storing landscape foundation types
public enum HexagonLandscapeFoundationType
{
    Flat,
    Rough,
    NUM_LS_FOUNDATIONTYPES
}
// enum storing climate foundation types
public enum HexagonClimateFoundationType
{
    Desert,
    Arid,
    Tempered,
    SubTropical,
    Tropical,
    Snow,
    NUM_CL_FOUNDATIONTYPES
}
// enum storing future types that can vary over time (forests, villages etc...)
public enum HexagonTypes
{
    Forest = 0,
    Village,
    NUM_HEXTYPES
}