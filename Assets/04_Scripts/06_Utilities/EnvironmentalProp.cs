﻿using UnityEngine;

// Class for EnvironmentalPropData
// This class stores location and rotation as well as type, variation and subtype of a prop
// This is the data ultimately stored to file when saved
[System.Serializable]
public class EnvironmentalPropData
{
    public EnvironmentalPropData(PrefabType type, PrefabSubType subType, int variationID = 0, Vector3 offsetLocation = new Vector3(), Quaternion localRotation = new Quaternion())
    {
        this.type = type;
        this.subType = subType;
        this.offsetLocation = offsetLocation;
        this.localRotation = localRotation;
        this.variationID = variationID;
    }
    public PrefabType type;
    public PrefabSubType subType;
    public int variationID;
    public Vector3 offsetLocation;
    public Quaternion localRotation;
}
// This is the Monobehaviour attached to the unity prop when loaded
// It contains the data stored in the EnvironmentalPropData
public class EnvironmentalProp : MonoBehaviour
{
    public EnvironmentalPropData Data;
    public PrefabType Type
    {
        get { return Data.type; }
        set { Data.type = value; }
    }

    public int VariationID
    {
        get { return Data.variationID; }
        set { Data.variationID = value; }
    }
    public PrefabSubType SubType
    {
        get { return Data.subType; }
        set { Data.subType = value; }
    }

    public EnvironmentalPropFactory Manager;
    public void Destroy()
    {
        Manager.ReturnPrefab(this);
    }
}
