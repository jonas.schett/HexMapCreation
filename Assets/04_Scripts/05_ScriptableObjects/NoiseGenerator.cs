﻿using System.Collections.Generic;
using UnityEngine;

// A noise generator for generating all the terrain noise of hexagons
// It is a scriptable object where different values can be stored
// The noise generator uses animationcurves to map the output of the
// noise function to the actual vertex height
// Those animationcurves can be defined by the user
[ExecuteInEditMode]
// CLass for generating noise for anything in the game
[CreateAssetMenu(fileName = "New NoiseGenerator", menuName = "NoiseGenerators/AveragingNoiseGenerator", order = 51)]
public class NoiseGenerator : ScriptableObject {

    #region Fields
    [SerializeField]
    int octaves;
    [SerializeField]
    float lacunarity;
    [SerializeField]
    float persistance;
    [SerializeField]
    float frequencyModifier;
    [SerializeField]
    float amplitudeModifier;
    [SerializeField]
    float horizontalMod;
    [SerializeField]
    AnimationCurve seaAnim;
    [SerializeField]
    AnimationCurve coastAnim;
    [SerializeField]
    AnimationCurve lowlandAnim;
    [SerializeField]
    AnimationCurve midlandAnim;
    [SerializeField]
    AnimationCurve highlandAnim;
    [SerializeField]
    AnimationCurve mountainAnim;

    [SerializeField]
    AnimationCurve roughAnim;
    [SerializeField]
    AnimationCurve flatAnim;
    [SerializeField]

    AnimationCurve deepSeaAnim;
    AnimationCurve highMountainAnim;
    private float seed = 20000f;

    Dictionary<HexagonElevantionFoundationType, AnimationCurve> elevationAnimationCurves;
    Dictionary<HexagonLandscapeFoundationType, AnimationCurve> landscapeAnimationCurves;

    public static float gradient = 1.7320508075688f;
    public static float northOrientationGradient = 0.57735026919f;
    private UnitVectors unitVectors;

    private bool vertexNorthOriented = false;

    #endregion

    #region Setup and Initialisation
    // Copy constructor
    public void TransferData(NoiseGenerator ng, bool vertexNorthOriented = false)
    {
        this.vertexNorthOriented = vertexNorthOriented;
        unitVectors = ng.unitVectors;
        octaves = ng.octaves;
        lacunarity = ng.lacunarity;
        persistance = ng.persistance;
        frequencyModifier = ng.frequencyModifier;
        amplitudeModifier = ng.amplitudeModifier;
        horizontalMod = ng.horizontalMod;

        Keyframe[] deepSeaAnimKeys = ng.seaAnim.keys;
        for(var i = 0; i < deepSeaAnimKeys.Length; i ++)
        {
            deepSeaAnimKeys[i].value -= 0.5f;
        }

        Keyframe[] highMountainAnimKeys = ng.mountainAnim.keys;
        for (var i = 0; i < highMountainAnimKeys.Length; i++)
        {
            highMountainAnimKeys[i].value += 0.5f;
        }

        highMountainAnim = new AnimationCurve(highMountainAnimKeys);
        deepSeaAnim = new AnimationCurve(deepSeaAnimKeys);
        seaAnim = new AnimationCurve(ng.seaAnim.keys);
        coastAnim = new AnimationCurve(ng.coastAnim.keys);
        lowlandAnim = new AnimationCurve(ng.lowlandAnim.keys);
        midlandAnim = new AnimationCurve(ng.midlandAnim.keys);
        highlandAnim = new AnimationCurve(ng.highlandAnim.keys);
        mountainAnim = new AnimationCurve(ng.mountainAnim.keys);
        roughAnim = new AnimationCurve(ng.roughAnim.keys);
        flatAnim = new AnimationCurve(ng.flatAnim.keys);

        InitialiseAnimationCurveDicts();
    }

    // function to set the standard unitVectors for the current scene
    public void SetUnitVectors(UnitVectors unitVectors)
    {
        this.unitVectors = unitVectors;

        InitialiseAnimationCurveDicts();
    }

    private void InitialiseAnimationCurveDicts()
    {
        elevationAnimationCurves = new Dictionary<HexagonElevantionFoundationType, AnimationCurve>();

        elevationAnimationCurves.Add(HexagonElevantionFoundationType.Sea, seaAnim);
        elevationAnimationCurves.Add(HexagonElevantionFoundationType.Coast, coastAnim);
        elevationAnimationCurves.Add(HexagonElevantionFoundationType.Lowlands, lowlandAnim);
        elevationAnimationCurves.Add(HexagonElevantionFoundationType.Midlands, midlandAnim);
        elevationAnimationCurves.Add(HexagonElevantionFoundationType.Highlands, highlandAnim);
        elevationAnimationCurves.Add(HexagonElevantionFoundationType.Mountain, mountainAnim);
        elevationAnimationCurves.Add(HexagonElevantionFoundationType.DeepSea, deepSeaAnim);
        elevationAnimationCurves.Add(HexagonElevantionFoundationType.HighMountain, highMountainAnim);

        landscapeAnimationCurves = new Dictionary<HexagonLandscapeFoundationType, AnimationCurve>();
        landscapeAnimationCurves.Add(HexagonLandscapeFoundationType.Rough, roughAnim);
        landscapeAnimationCurves.Add(HexagonLandscapeFoundationType.Flat, flatAnim);
    }

    #endregion

    #region Vertex noise calculations
    // Function adds noise to each vertex of a mesh depending on its x and z location 
    // It can also add location noise to the x and z loation by using the orthogonal axes.
    // not used at the moment, simplest noise generation
    public Vector3[] AddLocationNoiseToMesh(Vector3[] vertices, Vector3 location)
    {
        for(int i = 0; i < octaves; i++)
        {
            
            // calculates amplitude and frequency of the noise that is about to get generated
            float amplitude = Mathf.Pow(persistance, i) * amplitudeModifier;
            float frequency = Mathf.Pow(lacunarity, i) * frequencyModifier;

            // This loop adds noise to the y location of all vertices
            for (int j = 0; j < vertices.Length; j++)
            {
                float pX = ((vertices[j].x + location.x) * frequency) + seed;
                float pZ = ((vertices[j].z + location.z) * frequency) + seed;

                vertices[j].y = vertices[j].y + (lowlandAnim.Evaluate(Mathf.PerlinNoise(pX, pZ)-0.5f) * amplitude);
            }

            //This adds noise to x location
            for (int j = 0; j < vertices.Length; j++)
            {
                float pZ = ((vertices[j].z + location.z) * frequency) + seed;
                float pY = ((vertices[j].y + location.y) * frequency) + seed;

                vertices[j].x = vertices[j].x + ((Mathf.PerlinNoise(pY, pZ) - 0.5f) * amplitude*horizontalMod);
            }
            //This one to z location
            for (int j = 0; j < vertices.Length; j++)
            {
                float pX = ((vertices[j].x + location.x) * frequency) + seed;
                float pY = ((vertices[j].y + location.y) * frequency) + seed;

                vertices[j].z = vertices[j].z + ((Mathf.PerlinNoise(pY, pX) - 0.5f) * amplitude *horizontalMod);
            }
        }
        
        return vertices;
    }

    //This function is an advances funcion that will allow adding noise to a single vertex, depending on its position
    // It also has the functionality to merge the loc   ation with another location depending on the 
    // lerpModifier. 
    // also not used at the moment
    public Vector3 AddLocationNoiseToVertex(Vector3 vertex, Vector3[] mergeVector, Vector3 currentVector, float lerpFactor, bool enableMerge)
    {
        Vector3 vert = vertex;
        for (int i = 0; i < octaves; i++)
        {

            float amplitude = Mathf.Pow(persistance, i) * amplitudeModifier;
            float frequency = Mathf.Pow(lacunarity, i) * frequencyModifier;

            float pX = ((vert.x + currentVector.x) * frequency) + seed;
            float pZ = ((vert.z + currentVector.z) * frequency) + seed;
            float pY = ((vert.y + currentVector.y) * frequency) + seed;

            float pX2;
            float pZ2;
            float pY2;

            float localNoise = (lowlandAnim.Evaluate(Mathf.PerlinNoise(pX, pZ) - 0.5f) * amplitude);
            float localXNoise = (lowlandAnim.Evaluate(Mathf.PerlinNoise(pY, pZ) - 0.5f) * amplitude);
            float localZNoise = (lowlandAnim.Evaluate(Mathf.PerlinNoise(pX, pY) - 0.5f) * amplitude);

            float totalNoise = localNoise;
            float totalXNoise = localXNoise;
            float totalZNoise = localZNoise;

            // this checks if the merge is actually enabled.
            // It is not enabled when not necessary.
            if (enableMerge)
            {
                foreach(var member in mergeVector)
                {
                    pX2 = ((vert.x + member.x) * frequency) + seed;
                    pZ2 = ((vert.z + member.z) * frequency) + seed;
                    pY2 = ((vert.y + member.y) * frequency) + seed;

                    totalNoise += (lowlandAnim.Evaluate(Mathf.PerlinNoise(pX2, pZ2) - 0.5f) * amplitude);
                    totalXNoise += (lowlandAnim.Evaluate(Mathf.PerlinNoise(pY2, pZ2) - 0.5f) * amplitude);
                    totalZNoise += (lowlandAnim.Evaluate(Mathf.PerlinNoise(pX2, pY2) - 0.5f) * amplitude);
                }
                totalNoise = totalNoise / (mergeVector.Length +1);
                totalXNoise = totalXNoise / (mergeVector.Length + 1);
                totalZNoise = totalZNoise / (mergeVector.Length + 1);

                vert.y = vert.y + Mathf.Lerp(localNoise, totalNoise, lerpFactor);
                vert.x = vert.x + Mathf.Lerp(localXNoise, totalXNoise, lerpFactor) * horizontalMod;
                vert.z = vert.z + Mathf.Lerp(localZNoise, totalZNoise, lerpFactor) * horizontalMod;
            }
            else
            {
                vert.y = vert.y + localNoise;
                vert.x = vert.x + localXNoise*horizontalMod;
                vert.z = vert.z + localZNoise*horizontalMod;
            }
            // This will generate the noise. The first value is the noiseof the current location
            // The second value is the average noise of the current location and the merge location
            // The last value is the lerp factor which decides how far the noise is going in either direction. 
        }
        return vert;
    }


    //main noise generating function working on a vertex basis. Includes increased averaging depending on distance to edge of map.
    public Vector3 AddLocationNoiseToVertexStrongAveraging(Vector3 vertex, GridInformation GridInfo, Vector3 offset, AnimationCurve anim, AnimationCurve specialAnim)
    {
        Vector3 vert = vertex;
        for (int i = 0; i < octaves; i++)
        {
            // amplitude and frequency prameters of the noise
            float amplitude = Mathf.Pow(persistance, i) * amplitudeModifier;
            float frequency = Mathf.Pow(lacunarity, i) * frequencyModifier;

            // local noise parameters
            float pX = ((vert.x + GridInfo.AbsoluteLocation.x + offset.x) * frequency) + seed;
            float pZ = ((vert.z + GridInfo.AbsoluteLocation.z + offset.z) * frequency) + seed;

            // these represent the noise from the relative x location (x)
            float pXRelativeX = ((vert.x + GridInfo.RelativeXLocation.x + offset.x) * frequency) + seed;
            float pZRelativeX = ((vert.z + GridInfo.RelativeXLocation.z + offset.z) * frequency) + seed;

            // these represent the noise from the relative y location (z)
            float pXRelativeZ = ((vert.x + GridInfo.RelativeYLocation.x + offset.x) * frequency) + seed;
            float pZRelativeZ = ((vert.z + GridInfo.RelativeYLocation.z + offset.z) * frequency) + seed;
            

            // all these represent noise in the y direction (up) and the noises are combined to give a total noise
            float localYNoise = Mathf.PerlinNoise(pX, pZ);
            float localYNoiseX = Mathf.PerlinNoise(pXRelativeX, pZRelativeX);
            float localYNoiseZ = Mathf.PerlinNoise(pXRelativeZ, pZRelativeZ);

            // y noise averaging
            float YnoiseX = Mathf.Lerp(localYNoise, localYNoiseX, GridInfo.DistanceXToCentreLerp);
            float YnoiseZ = Mathf.Lerp(localYNoise, localYNoiseZ, GridInfo.DistanceYToCentreLerp);

            float totalYNoise = anim.Evaluate((YnoiseX + YnoiseZ) /2f) * amplitude;
            totalYNoise += specialAnim.Evaluate((YnoiseX + YnoiseZ) / 2f) * amplitude;
            // assigns the vertical noise to the vertex
            vert.y += totalYNoise;

            if (horizontalMod != 0)
            {
                // relative locations for x and z noise
                // y direction
                float pY = ((vert.y + GridInfo.AbsoluteLocation.y) * frequency) + seed;
                // x direction
                float pYRelativeX = ((vert.y + GridInfo.RelativeXLocation.y + offset.y) * frequency) + seed;
                // z direction
                float pYRelativeZ = ((vert.y + GridInfo.RelativeYLocation.y + offset.y) * frequency) + seed;

                // the below is only happening if we do horizontal noise as well
                // noise affecting x part of the vertex
                float localXNoise = Mathf.PerlinNoise(pY, pZ) - 0.5f;
                float localXNoiseX = Mathf.PerlinNoise(pYRelativeX, pZRelativeX) - 0.5f;
                float localXNoiseZ = Mathf.PerlinNoise(pYRelativeZ, pZRelativeZ) - 0.5f;

                // noise affecting z part of the vertex
                float localZNoise = Mathf.PerlinNoise(pX, pY) - 0.5f;
                float localZNoiseX = Mathf.PerlinNoise(pXRelativeX, pYRelativeX) - 0.5f;
                float localZNoiseZ = Mathf.PerlinNoise(pXRelativeZ, pYRelativeZ) - 0.5f;

                // x noise averaging
                float XnoiseX = Mathf.Lerp(localXNoise, localXNoiseX, GridInfo.DistanceXToCentreLerp);
                float XnoiseZ = Mathf.Lerp(localXNoise, localXNoiseZ, GridInfo.DistanceYToCentreLerp);

                float totalXNoise = /*anim.Evaluate*/((XnoiseX + XnoiseZ) / 2f) * horizontalMod; //* totalYNoise;

                // z noise averaging
                float ZnoiseX = Mathf.Lerp(localZNoise, localZNoiseX, GridInfo.DistanceXToCentreLerp);
                float ZnoiseZ = Mathf.Lerp(localZNoise, localZNoiseZ, GridInfo.DistanceYToCentreLerp);

                float totalZNoise = /*anim.Evaluate*/((ZnoiseX + ZnoiseZ) / 2f) * horizontalMod; //* totalYNoise;

                // assign the horizontal noise to the vertex
                vert.x += totalXNoise;
                vert.z += totalZNoise;
            }
        }
        return vert;
    }

    // Function adds location noise to the vertex without considering the relative grid location
    public Vector3 AddLocationNoiseToVertexNoRelativeGridLocation(Vector3 vertex, GridInformation GridInfo, Vector3 offset, AnimationCurve anim, AnimationCurve specialAnim)
    {
        Vector3 vert = vertex;
        for (int i = 0; i < octaves; i++)
        {
            // amplitude and frequency prameters of the noise
            float amplitude = Mathf.Pow(persistance, i) * amplitudeModifier;
            float frequency = Mathf.Pow(lacunarity, i) * frequencyModifier;

            // local noise parameters
            float pX = ((vert.x + GridInfo.AbsoluteLocation.x + offset.x) * frequency) + seed;
            float pZ = ((vert.z + GridInfo.AbsoluteLocation.z + offset.z) * frequency) + seed;

            float localYNoise = Mathf.PerlinNoise(pX, pZ);


            float totalYNoise = anim.Evaluate(localYNoise) * amplitude;
            totalYNoise += specialAnim.Evaluate(localYNoise) * amplitude;

            vert.y += totalYNoise;

            if (horizontalMod != 0)
            {
                // local y noise parameter
                float pY = ((vert.y + GridInfo.AbsoluteLocation.y) * frequency) + seed;


                // the below is only happening if we do horizontal noise as well
                // noise affecting x part of the vertex
                float localXNoise = Mathf.PerlinNoise(pY, pZ) - 0.5f;

                // noise affecting z part of the vertex
                float localZNoise = Mathf.PerlinNoise(pX, pY) - 0.5f;

                // assign the horizontal noise to the vertex
                vert.x += localXNoise;
                vert.z += localZNoise;
            }
        }
        return vert;
    }

    // calculates noise for a vertex
    private Vector3 VertexNoiseCalculation(Vector3 current, Hexagon mergeOne, Vector3 offsetMergeOne, Hexagon mergeTwo, Vector3 offsetMergeTwo, Hexagon mergeThree, Vector3 offsetMergeThree, bool corner, float distance, Hexagon baseHexagon, double vertexAngle)
    {
        // TODO add functionality for vector to be averaged over 3 neighbours next to current hex
        Vector3 noiseBase;
        Vector3 noiseMergeOne;
        Vector3 noiseMergeTwo;

        noiseBase = AddLocationNoiseToVertexNoRelativeGridLocation(current, baseHexagon.GridInfo, new Vector3(0, 0, 0), 
            elevationAnimationCurves[baseHexagon.ElevationFoundationType], 
            landscapeAnimationCurves[baseHexagon.LandscapeFoundationType]);

        noiseMergeOne = AddLocationNoiseToVertexNoRelativeGridLocation(current, mergeOne.GridInfo, offsetMergeOne, 
            elevationAnimationCurves[mergeOne.ElevationFoundationType],
            landscapeAnimationCurves[mergeOne.LandscapeFoundationType]);

        if(vertexAngle > 0)
        {
            noiseMergeTwo = AddLocationNoiseToVertexNoRelativeGridLocation(current, mergeThree.GridInfo, offsetMergeThree, 
                elevationAnimationCurves[mergeThree.ElevationFoundationType],
                landscapeAnimationCurves[mergeThree.LandscapeFoundationType]);
        }
        else
        {
            noiseMergeTwo = AddLocationNoiseToVertexNoRelativeGridLocation(current, mergeTwo.GridInfo, offsetMergeTwo, 
                elevationAnimationCurves[mergeTwo.ElevationFoundationType],
                landscapeAnimationCurves[mergeTwo.LandscapeFoundationType]);
        }

        if (!corner)
        {
            float lerpAngle = Mathf.Abs((float)vertexAngle) / 30f;
            var noiseAveraging = (noiseBase + noiseMergeOne) / 2f;
            var noiseCornerAveraging = (noiseBase + noiseMergeOne + noiseMergeTwo) / 3f;
            var noiseTargetAveraging = Vector3.Lerp(noiseAveraging, noiseCornerAveraging, Mathf.Pow(Mathf.Clamp01(lerpAngle), 2.5f));
            current = Vector3.Lerp(noiseBase, noiseTargetAveraging, Mathf.Pow(Mathf.Clamp01(distance), 2.5f));
            //current = (noiseBase + noiseAveraging) / 2f;
        }
        else
        {
            current = (noiseBase + noiseMergeOne + noiseMergeTwo) / 3f;
        }
        //current = (noiseBase + noiseMergeOne) / 2f;

        return current;
    }

    // Method adds noise to a single hexagon, the noise added will depend on the area of the hexagon it is in
    // The closer it comes to the edge, the more it will be averaged
    public Vector3[] AddNoiseToHexagon(Hexagon hex, Vector3[] vertices)
    {
        Neighbours neighbours = hex.GetNeighbours();
        GridInformation GridInfo = hex.GetGridInformation();
        float distanceOne;
        float distanceTwo;
        // ensure the amplitudemodifier takes into account the change in persistance
        amplitudeModifier = amplitudeModifier / persistance;

        for (int i = 0; i < vertices.Length; i++)
        {
            
            var corner = false;

            if (Mathf.Approximately(vertices[i].magnitude, GridInfo.OuterRadius))
            {
                corner = true;
            }

            if (!vertexNorthOriented)
            {
                distanceOne = (Mathf.Abs(vertices[i].z) + gradient * Mathf.Abs(vertices[i].x)) / (2f * GridInfo.InnerRadius);
                distanceTwo = Mathf.Abs(vertices[i].z) / GridInfo.InnerRadius;
                var vertexAngle = NumberConversions.Rad2Deg(Mathf.Atan2(vertices[i].z, vertices[i].x));
                if (vertexAngle <= -119.9f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.SW /*SW*/, unitVectors.FromSouthWest,
                        neighbours.NW /*NW*/, unitVectors.FromNorthWest,
                        neighbours.S  /*S */, unitVectors.FromSouth,
                        corner, distanceOne, hex, vertexAngle + 150);
                }
                else if (vertexAngle <= -59.9f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.S  /*S */, unitVectors.FromSouth,
                        neighbours.SW /*SW*/, unitVectors.FromSouthWest,
                        neighbours.SE /*SE*/, unitVectors.FromSouthEast,
                        corner, distanceTwo, hex, vertexAngle + 90);
                }
                else if (vertexAngle <= 0.1f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.SE /*SE*/, unitVectors.FromSouthEast,
                        neighbours.S  /*S */, unitVectors.FromSouth,
                        neighbours.NE /*NW*/, unitVectors.FromNorthEast,
                        corner, distanceOne, hex, vertexAngle + 30);
                }
                else if (vertexAngle <= 60.1f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.NE /*NE*/, unitVectors.FromNorthEast,
                        neighbours.SE /*SE*/, unitVectors.FromSouthEast,
                        neighbours.N  /*N */, unitVectors.FromNorth,
                        corner, distanceOne, hex, vertexAngle - 30);
                }
                else if (vertexAngle <= 120.1f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.N  /*N */, unitVectors.FromNorth,
                        neighbours.NE /*NE*/, unitVectors.FromNorthEast,
                        neighbours.NW /*NW*/, unitVectors.FromNorthWest,
                        corner, distanceTwo, hex, vertexAngle - 90);
                }
                else
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.NW /*NW*/, unitVectors.FromNorthWest,
                        neighbours.N  /*N */, unitVectors.FromNorth,
                        neighbours.SW /*SW*/, unitVectors.FromSouthWest,
                        corner, distanceOne, hex, vertexAngle - 150);
                }
            }
            else if(true)
            {
                distanceOne = (Mathf.Abs(vertices[i].x) + gradient * Mathf.Abs(vertices[i].z)) / (2f* GridInfo.InnerRadius);
                distanceTwo = Mathf.Abs(vertices[i].x) / GridInfo.InnerRadius;
                var vertexAngle = NumberConversions.Rad2Deg(Mathf.Atan2(-vertices[i].x, vertices[i].z));
                if (vertexAngle <= -119.9f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.SE /*SE*/, unitVectors.FromSouthEast,
                        neighbours.SW /*SW*/, unitVectors.FromSouthWest,
                        neighbours.E  /*E */, unitVectors.FromEast,
                        corner, distanceOne, hex, vertexAngle + 150);
                }
                else if (vertexAngle <= -59.9f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.E  /*E */, unitVectors.FromEast,
                        neighbours.SE /*SE*/, unitVectors.FromSouthEast,
                        neighbours.NE /*NE*/, unitVectors.FromNorthEast,
                        corner, distanceTwo, hex, vertexAngle + 90);
                }
                else if (vertexAngle <= 0.1f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.NE /*NE*/, unitVectors.FromNorthEast,
                        neighbours.E  /*E */, unitVectors.FromEast,
                        neighbours.NW /*NW*/, unitVectors.FromNorthWest,
                        corner, distanceOne, hex, vertexAngle + 30);
                }
                else if (vertexAngle <= 60.1f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.NW /*NW*/, unitVectors.FromNorthWest,
                        neighbours.NE /*NE*/, unitVectors.FromNorthEast,
                        neighbours.W  /*W */, unitVectors.FromWest,
                        corner, distanceOne, hex, vertexAngle - 30);
                }
                else if (vertexAngle <= 120.1f)
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.W  /*W */, unitVectors.FromWest,
                        neighbours.NW /*NW*/, unitVectors.FromNorthWest,
                        neighbours.SW /*SW*/, unitVectors.FromSouthWest,
                        corner, distanceTwo, hex, vertexAngle - 90);
                }
                else
                {
                    vertices[i] = VertexNoiseCalculation(vertices[i],
                        neighbours.SW /*SW*/, unitVectors.FromSouthWest,
                        neighbours.W  /*W */, unitVectors.FromWest,
                        neighbours.SE /*SE*/, unitVectors.FromSouthEast,
                        corner, distanceOne, hex, vertexAngle - 150);
                }
            }
        }

        return vertices;
    }
    #endregion

    #region Base type noise calculations
    // Method adds base types to hexagons, this is also done using noise based on the location
    public static void AddBaseTypesToMapSingleNoise(List<Hexagon> hexagons, float baseTypeFrequencyMultiplier)
    {
        float seed = Random.Range(0.0f, 1.0f);
        foreach(Hexagon hex in hexagons)
        {
            float noise = GenerateGridLocationNoise(hex, seed, baseTypeFrequencyMultiplier);
            //noise *= (int)HexagonBaseTypes.NUM_BASETYPES;
            if (noise < 0.5f)
            {
                hex.ElevationFoundationType = HexagonElevantionFoundationType.Sea;
            }
            else if(noise < 0.55f)
            {
                hex.ElevationFoundationType = HexagonElevantionFoundationType.Coast;
            }
            else if(noise < 0.65f)
            {
                hex.ElevationFoundationType = HexagonElevantionFoundationType.Lowlands;
            }
            else if (noise < 0.75f)
            {
                hex.ElevationFoundationType = HexagonElevantionFoundationType.Midlands;
            }
            else if (noise < 0.8f)
            {
                hex.ElevationFoundationType = HexagonElevantionFoundationType.Highlands;
            }
            else
            {
                hex.ElevationFoundationType = HexagonElevantionFoundationType.Mountain;
            }
        }

        foreach(Hexagon hex in hexagons)
        {
            ChangeHexagonElevationTypeBasedOnNeighbours(hex, HexagonElevantionFoundationType.Sea, HexagonElevantionFoundationType.DeepSea);
            ChangeHexagonElevationTypeBasedOnNeighbours(hex, HexagonElevantionFoundationType.Mountain, HexagonElevantionFoundationType.HighMountain);
        }

        seed = Random.Range(0.0f, 1.0f);
        foreach (Hexagon hex in hexagons)
        {
            //float noise = Mathf.Clamp(Mathf.PerlinNoise(baseTypeNoiseGenerationXComponent, baseTypeNoiseGenerationYComponent),0f,0.9999f);
            float noise = GenerateGridLocationNoise(hex, seed, baseTypeFrequencyMultiplier);
            //noise *= (int)HexagonBaseTypes.NUM_BASETYPES;
            if (noise < 0.5f)
            {
                hex.LandscapeFoundationType = HexagonLandscapeFoundationType.Flat;
            }
            else
            {
                hex.LandscapeFoundationType = HexagonLandscapeFoundationType.Rough;
            }
        }

        seed = Random.Range(0.0f, 1.0f);
        foreach (Hexagon hex in hexagons)
        {

            float noise = GenerateGridLocationNoise(hex, seed, baseTypeFrequencyMultiplier);
            //noise *= (int)HexagonBaseTypes.NUM_BASETYPES;
            if (noise < 0.2)
            {
                hex.ClimateFoundationType = HexagonClimateFoundationType.Desert;
            }
            else if(noise < 0.4)
            {
                hex.ClimateFoundationType = HexagonClimateFoundationType.Arid;
            }
            else if(noise < 0.6)
            {
                hex.ClimateFoundationType = HexagonClimateFoundationType.Tempered;
            }
            else if (noise < 0.8)
            {
                hex.ClimateFoundationType = HexagonClimateFoundationType.SubTropical;
            }
            else
            {
                hex.ClimateFoundationType = HexagonClimateFoundationType.Tropical;
            }
        }
    }

    // This function will update the elevation of nearby hexagons depending on all neighbours of that hexagon
    // It is used to get deep sea or high mountain hexagon types
    private static void ChangeHexagonElevationTypeBasedOnNeighbours(Hexagon hex, HexagonElevantionFoundationType baseType, HexagonElevantionFoundationType targetType)
    {
        if (hex.ElevationFoundationType == baseType)
        {
            bool shouldBeDeepSea = true;
            foreach (Hexagon neighbour in hex.GetNeighbours().GetArray())
            {
                if (neighbour.ElevationFoundationType != baseType && neighbour.ElevationFoundationType != targetType)
                {
                    shouldBeDeepSea = false;
                    break;
                }
            }
            if (shouldBeDeepSea)
            {
                hex.ElevationFoundationType = targetType;
            }
        }
    }

    // This function will generate the base types for hexagons depending on their location on the map using noise
    private static float GenerateGridLocationNoise(Hexagon hex, float seed, float frequencyModifier)
    {
        // these represent the noise from the actual location
        float baseTypeNoiseGenerationXComponent = (seed + hex.GridInfo.GridLocation.x / hex.GridInfo.GridSize) * frequencyModifier;
        float baseTypeNoiseGenerationYComponent = (seed + hex.GridInfo.GridLocation.y / hex.GridInfo.GridSize) * frequencyModifier;

        // relative x location
        float baseTypeRelativeX = (seed + hex.GridInfo.RelativeGridLocation.x / hex.GridInfo.GridSize) * frequencyModifier;

        // relative y location
        float baseTypeRelativeY = (seed + hex.GridInfo.RelativeGridLocation.y / hex.GridInfo.GridSize) * frequencyModifier;

        float localYNoiseX = Mathf.Clamp(Mathf.PerlinNoise(baseTypeRelativeX, baseTypeNoiseGenerationYComponent), 0f, 0.9999f);
        float localYNoiseZ = Mathf.Clamp(Mathf.PerlinNoise(baseTypeNoiseGenerationXComponent, baseTypeRelativeY), 0f, 0.9999f);

        float localYNoise = Mathf.Clamp(Mathf.PerlinNoise(baseTypeNoiseGenerationXComponent, baseTypeNoiseGenerationYComponent), 0f, 0.9999f);

        // relative noise averaging
        float YnoiseX = Mathf.Lerp(localYNoise, localYNoiseX, hex.GridInfo.DistanceXToCentreLerp);
        float YnoiseZ = Mathf.Lerp(localYNoise, localYNoiseZ, hex.GridInfo.DistanceYToCentreLerp);


        //float noise = Mathf.Clamp(Mathf.PerlinNoise(baseTypeNoiseGenerationXComponent, baseTypeNoiseGenerationYComponent),0f,0.9999f);
        return (YnoiseX + YnoiseZ + localYNoise) / 3f;
    }
    #endregion

    #region VertexCorrection

    // this function loops through all vertices and corrects them so they will tend towards 0 at the edge.
    // this is done by comparing them to the edge lines and lerping to 0 the closer the vertex is to it.
    //formula for conversion:
    // |-z0 + mx0|/SQRT(m*m+1*1)
    public void CorrectVertices(Hexagon hex)
    {
        float gradientOne = -1.73205080756888f;

        float gradeientTwo = -gradientOne;
        float correctionFactor;
        double vertexAngle;
        Vector3[] vertices = hex.GetMesh().vertices;

        for (int i = 0; i < hex.GetMesh().vertices.Length; i++)
        {
            vertexAngle = NumberConversions.Rad2Deg(Mathf.Atan2(vertices[i].z, vertices[i].x));
            if (vertexAngle < -120)
            {
                correctionFactor = Mathf.Abs(-vertices[i].z + gradientOne * vertices[i].x) / 2f;
            }
            else if (vertexAngle < -60)
            {
                correctionFactor = Mathf.Abs(vertices[i].z);

            }
            else if (vertexAngle < 0)
            {
                correctionFactor = Mathf.Abs(-vertices[i].z + gradeientTwo * vertices[i].x) / 2f;
            }
            else if (vertexAngle < 60)
            {
                correctionFactor = Mathf.Abs(-vertices[i].z + gradientOne * vertices[i].x) / 2f;
            }
            else if (vertexAngle < 120)
            {
                correctionFactor = Mathf.Abs(vertices[i].z);
            }
            else
            {
                correctionFactor = Mathf.Abs(-vertices[i].z + gradeientTwo * vertices[i].x) / 2f;
            }

            vertices[i].y = Mathf.Lerp(vertices[i].y, 0f, correctionFactor / hex.GridInfo.InnerRadius);
        }
        hex.GetMesh().vertices = vertices;
    }

    public void CorrectHexagonVertexBorders(IEnumerable<Hexagon> hexagons)
    {
        foreach (Hexagon hex in hexagons)
        {
            Vector3[] vertices = hex.GetMesh().vertices;
            Hexagon[] neighboursList = hex.GetNeighbours().GetArray();
            int neighboursIndex = 0;

            for (int i = 0; i < hex.HexagonIterations * 6; i++)
            {
                if (i % hex.HexagonIterations == 0 && i != 0)
                {
                    neighboursIndex++;
                }
                if (neighboursIndex + 1 <= neighboursList.Length)
                {
                    // counts on which vertex of the current edge we are
                    int edgeVertexCount = i % hex.HexagonIterations;
                    // count of total vertices on the edge of hexagon
                    int totalEdgeVertexCount = hex.HexagonIterations * 6;

                    int mirroredIndex = (i + 4 * hex.HexagonIterations - 2 * edgeVertexCount) % totalEdgeVertexCount;
                    int completeMirroredIndex = hex.FirstEdgeVertexIndex + mirroredIndex;
                    int completeIndex = hex.FirstEdgeVertexIndex + i;
                    if (completeMirroredIndex + 1 <= neighboursList[neighboursIndex].GetMesh().vertices.Length)
                    {
                        vertices[completeIndex] = neighboursList[neighboursIndex].GetMesh().vertices[completeMirroredIndex];
                    }

                }

            }
            hex.GetMesh().vertices = vertices;
        }
    }
    #endregion

    #region Prop placement noise calculation
    public static float GenerateNoiseForPropPlacement(Vector3 position, float frequency, float seed, int octaves = 1)
    {
        float noise = 0;
        float multiplier = InverseFactorial(octaves);
        for (int i = 1; i <= octaves; i ++)
        {
            frequency *= i;
            float noiseForOctave = ((Random.Range(0f, 1f) * 0.25f) + Mathf.PerlinNoise(position.x * frequency + seed, position.z * frequency + seed) * 0.75f) / i;
            noise += noiseForOctave / multiplier;
        }
        return noise;
    }
    #endregion

    #region Utilities
    private static float Factorial(int i)
    {
        return i <= 0 ? 0 : Factorial(i - 1) + i;
    }

    private static float InverseFactorial(int i)
    {
        return i <= 0 ? 0 : InverseFactorial(i-1) + 1f / i;
    }
    #endregion
}