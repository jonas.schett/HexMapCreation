﻿using UnityEngine;

// Static maths utilities class
public static class NumberConversions {

    private static double PI = 3.14159265359;

    public static double Rad2Deg(double input)
    {
        return input * 180 / PI;
    }
    public static double Deg2Rad(double input)
    {
        return input * PI / 180;

    }

    public static int mod(int a, int b)
    {
        if(a >= 0)
        {
            return a % b;
        }
        else
        {
            return b + (a % b);
        }           
    }

    static float nfmod(float a, float b)
    {
        return a - b * Mathf.Floor(a / b);
    }
}
