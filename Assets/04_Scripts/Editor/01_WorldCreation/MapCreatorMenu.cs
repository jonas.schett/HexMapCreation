﻿#if (UNITY_EDITOR)

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

[ExecuteInEditMode]
public class MapCreatorMenu : HexagonEditor
{

    [MenuItem("Window/HexMapTools/MapCreator")]
    public static void ShowWindow()
    {
        GetWindow(typeof(MapCreatorMenu));
    }

    public override void OnGUI()
    {
        base.OnGUI();
        //GUILayout.Label("Generates a map");

        int verticalLocation = 95;
        vertexNorthOrientation = EditorGUI.Toggle(new Rect(3, verticalLocation+=23, position.width - 6, 20), "Vertex north oriented", vertexNorthOrientation);
        outerRadius = EditorGUI.FloatField(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Radius", outerRadius);
        gridSize = EditorGUI.IntField(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Grid Size", gridSize);
        BaseTypeFrequency = EditorGUI.FloatField(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Base Type Frequency", BaseTypeFrequency);
        //noiseGenerator = EditorGUILayout.ObjectField("", noiseGenerator, typeof(NoiseGenerator), true) as NoiseGenerator;

        if (hexagon)
        {
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width-6, 20),"Generate Map"))
            {
                Generate();
            }
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Save to file"))
            {
                Save();
            }
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Clear"))
            {
                DestroyOld();
            }
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Load from file"))
            {
                Load();
            }
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Recalculate updated hexagons"))
            {
                if(hexGrid != null)
                {
                    RecalculateChangedHexagons();
                }
                else
                {
                    Debug.LogError("Ensure you have a valid grid. Either generate one, or load one from file in order for it to be valid.");
                }
                
            }
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Add environmental props"))
            {
                AddEnvironmentalObjectsToMap();
            }
            if (GUI.Button(new Rect(3, verticalLocation += 23, position.width - 6, 20), "Recalculate all hexagons"))
            {
                RecalculateHexagons(hexagons,false);
            }
        }
    }
    public float BaseTypeFrequency;

    //bool whether this hexagon uses shared vertices or not
    

    Canvas gridCanvas;

    //this coroutine will spawn new hexagons
    private void CreateHexagons(int xOffset, int yOffset)
    {
        for(int i = 0; i < gridSize; i ++)
        {
            for(int j = 0; j < gridSize; j++)
            {
                GridInformation gridInfo = new GridInformation(gridSize, new Vector2(i + xOffset, j + yOffset), outerRadius, vertexNorthOrientation);
                hexGrid[i + xOffset, j + yOffset] = Instantiate(hexagon);
                // this code adds certain parameters to the hexagon and sets the map manager to be their parent

                hexGrid[i + xOffset, j + yOffset].GiveData(hexGrid,gridInfo);
                // this is a key component of the generation, as the hexagons will be generated on their location
                // this code takes care of transferring the current gridlocation to the world location
                hexGrid[i + xOffset, j + yOffset].transform.position = gridInfo.AbsoluteLocation;
                hexagons.Add(hexGrid[i + xOffset, j + yOffset]);
                hexGrid[i + xOffset, j + yOffset].ActivateInstant(environmentalObjectPlacer);

                trackingHexagonData[i + xOffset, j + yOffset] = hexGrid[i + xOffset, j + yOffset].GenerateHexagonData();
            }
        }

        NoiseGenerator.AddBaseTypesToMapSingleNoise(hexagons, BaseTypeFrequency);

        foreach (var hex in hexGrid)
        {
            //NoiseGenerator ng = new NoiseGenerator(noiseGenerator);
            NoiseGenerator ng = CreateInstance("NoiseGenerator") as NoiseGenerator;
            ng.TransferData(noiseGenerator,vertexNorthOrientation);
            Vector3[] hexVertices = hex.GetVertices();
            noiseGenerationTasks.Add(Task.Factory.StartNew(() => StartNoiseGeneration(ng, hexVertices, hex)));
        }

        foreach(var task in noiseGenerationTasks)
        {
            Task.WaitAll(task);
        }

        // recalculate all information inside the hexagon
        RecalculateHexagons(hexagons,true);
    }
    // function called by user when pressing the Generate button in the UI
    // it will generate a new map
    public void Generate()
    {
        DestroyOld();
        unitVectors.Initialise(outerRadius,innerRadius, vertexNorthOrientation);

        CreateHexagonLists();

        // give the current noiseGenerator the unitVectors
        noiseGenerator.SetUnitVectors(unitVectors);

        innerRadius = Mathf.Sqrt(outerRadius * outerRadius - (outerRadius / 2f) * (outerRadius / 2f));
        CreateHexagons(0, 0);
    }

    private void AddEnvironmentalObjectsToMap()
    {
        if(environmentalObjectPlacer != null)
        {
            // ensure we destroy all the previous 
            environmentalObjectPlacer.GetEnvironmentalObjectReferenceUtility().DestroyAll();
            foreach (Hexagon hex in hexGrid)
            {
                if(hex != null)
                {
                    environmentalObjectPlacer.AddObjectsToHexagon(hex, 0.05f);
                }
            }
        }
        else
        {
            Debug.LogError("Envornmental object placer is not present!");
        }
        
    }

    private void DeleteOldFiles()
    {
        DirectoryInfo di = new DirectoryInfo(GetMapDirectory());
        FileInfo[] files = di.GetFiles("*.hex").Where(p => p.Extension == ".hex").ToArray();

        foreach (FileInfo file in files)
            try
            {
                file.Attributes = FileAttributes.Normal;
                File.Delete(file.FullName);
            }
            catch
            {
                Debug.LogError("Problems deleting old hex files");
            }
    }

    public void Save()
    {
        // delete the previous files
        DeleteOldFiles();

        string directory = GetMapDirectory();
        string path = Path.Combine(directory, "mapInformation.hex");
        BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create));
        writer.Write(gridSize);
        writer.Write(outerRadius);
        writer.Write(vertexNorthOrientation);
        writer.Close();


        foreach (var hex in hexGrid)
        {
            if(hex != null)
            {
                hex.SaveToFile(directory);
            }
        }

        // destroy the gameobjects
        DestroyOld();
    }

    public void Load()
    {
        string directory = GetMapDirectory();
        string path = Path.Combine(directory, "mapInformation.hex");

        DestroyOld();

        LoadMapInformation(path);

        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                hexGrid[i, j].ActivateInstant(environmentalObjectPlacer);
                hexGrid[i, j].transform.position = hexGrid[i, j].GridInfo.AbsoluteLocation;
            }
        }
    }

    // function to add changes that have been applied to the hexagon in the menu to the physical appearance of the map
    // If a type is changed, whis will be accounted for in this piece of code
    
    
    // this function will destroy the previous map before generating a new one
    public void DestroyOld()
    {
        environmentalObjectPlacer.GetEnvironmentalObjectReferenceUtility().DestroyAll();
        if (hexagons != null)
        {
            hexagons.Clear();
        }
        foreach (var hex in FindObjectsOfType<Hexagon>())
        {
            DestroyImmediate(hex.gameObject, false);
        }
    }
}
#endif