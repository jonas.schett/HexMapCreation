﻿using UnityEngine;

// this class stores everything a hexagon needs to know about its location and about
// possible repeat locations. It will be used heavily for noise calculation
public class GridInformation
{
    public GridInformation(int gridSize, Vector2 location, float outerRadius, bool vertexNorthOrientation = false)
    {
        Initialise(gridSize, location, outerRadius, vertexNorthOrientation);
    }
    public Vector2 GridLocation { get; private set; }
    public Vector2 RelativeGridLocation { get; private set; }
    public Vector3 AbsoluteLocation { get; private set; }
    public Vector3 RelativeXLocation { get; private set; }
    public Vector3 RelativeYLocation { get; private set; }
    public int DistanceXToCentre { get; private set; }
    public int DistanceYToCentre { get; private set; }
    public float DistanceXToCentreLerp { get; private set; }
    public float DistanceYToCentreLerp { get; private set; }
    public int GridSize { get; private set; }
    public float OuterRadius { get; private set; }
    public float InnerRadius { get; private set; }
    public bool VertexNorthOriented { get; private set; }
    private void Initialise(int gridSize, Vector2 gridLocation, float hexSize, bool vertexNorthOriented = false)
    {
        VertexNorthOriented = vertexNorthOriented;
        OuterRadius = hexSize;
        InnerRadius = hexSize * 0.86602540378444f;
        GridLocation = gridLocation;
        GridSize = gridSize;
        int relX;
        int relY;

        DistanceXToCentre = (int)(GridLocation.x - (GridSize - 1) / 2);
        DistanceYToCentre = (int)(GridLocation.y - (GridSize - 1) / 2);

        if (DistanceXToCentre < 0)
        {
            //relX = (int)GridLocation.x + 2 * Mathf.Abs(DistanceXToCentre);
            relX = (int)GridLocation.x + GridSize;
        }
        else
        {
            //relX = (int)GridLocation.x - 2 * Mathf.Abs(DistanceXToCentre);
            relX = (int)GridLocation.x - GridSize;
        }

        DistanceXToCentre = Mathf.Abs(DistanceXToCentre);

        if (DistanceYToCentre < 0)
        {
            //relY = (int)GridLocation.x + 2 * Mathf.Abs(DistanceYToCentre);
            relY = (int)GridLocation.y + GridSize;
        }
        else
        {
            //relY = (int)GridLocation.x - 2 * Mathf.Abs(DistanceYToCentre);
            relY = (int)GridLocation.y - GridSize;
        }

        DistanceYToCentre = Mathf.Abs(DistanceYToCentre);

        DistanceXToCentreLerp = DistanceXToCentre == 0 ? 0f : (float)DistanceXToCentre / gridSize;
        DistanceYToCentreLerp = DistanceYToCentre == 0 ? 0f : (float)DistanceYToCentre / gridSize;

        RelativeGridLocation = new Vector2(relX, relY);

        float offset = (gridSize - 1) / 2f;

        if(VertexNorthOriented)
        {
            AbsoluteLocation =
            (GridLocation.x - offset) * new Vector3(2f * InnerRadius, 0, 0) +
            (GridLocation.y - offset) * new Vector3(InnerRadius, 0, 1.5f * OuterRadius);
        }
        else
        {
            AbsoluteLocation =
            (GridLocation.x - offset) * new Vector3(1.5f * OuterRadius, 0, -InnerRadius) +
            (GridLocation.y - offset) * new Vector3(0, 0, 2 * InnerRadius);
        }

        // two relative locations exist, as there are two possible relative locations for each hexagon, one in y direction and one in z direction
        if(VertexNorthOriented)
        {
            RelativeXLocation =
                (RelativeGridLocation.x - offset) * new Vector3(2 * InnerRadius, 0, 0) +
                (-offset) * new Vector3(InnerRadius, 0, 1.5f * OuterRadius);

            RelativeYLocation =
                (RelativeGridLocation.y - offset) * new Vector3(InnerRadius, 0, 1.5f * OuterRadius) +
                (-offset) * new Vector3(2 * InnerRadius, 0, 0);
        }
        else
        {
            RelativeXLocation =
            (RelativeGridLocation.x - offset) * new Vector3(1.5f * OuterRadius, 0, -InnerRadius) +
            (-offset) * new Vector3(0, 0, 2 * InnerRadius);

            RelativeYLocation =
                (-offset) * new Vector3(1.5f * OuterRadius, 0, -InnerRadius) +
                (RelativeGridLocation.y - offset) * new Vector3(0, 0, 2 * InnerRadius);
        }
        
    }
}