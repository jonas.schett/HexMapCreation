﻿using UnityEngine;
// enum describing all possible types of props
// if a new prop should be added, it needs to be added here
public enum PrefabType
{
    // Trees
    AcaiaTree01,
    AppleTree01,
    BirchTree01,
    FirTree01,
    OakTree01,
    PalmTree01,
    PineTree01,
    RandomTree01,
    SimpleTree01,
    ThujaTree01,

    Grass01,
    Grass02,
    Grass03,

    // Rocks
    Rock,
    NUM_TYPES,
    RANDOM,
    NONE
}

//subtyps of props
public enum PrefabSubType
{
    Fruit,
    Standard,
    Broken,
    Dead,
    Snow,
    NUM_SUBTYPES,
    RANDOM,
    NONE
}

// classes for environmental props, they store the information about props, such as their survival rate,
// actual prefabs, variations, types and subtypes
// A single type could have very many different props
[System.Serializable]
public class EnvironmentalPropSubType
{
    public GameObject subPrefab;
    public PrefabSubType subType;
}

[System.Serializable]
public class EnvironmentalPropVariation
{
    public EnvironmentalPropSubType[] subTypes;
}

[System.Serializable]
[CreateAssetMenu(fileName = "New EnvironmentalPropIdentifier", menuName = "PrefabManagement/EnvironmentalPropIdentifier", order = 51)]
public class EnvironmentalPropIdentifier : ScriptableObject
{
    public HexagonElevantionFoundationType[] elevationSurvivalTypes;
    public float[] elevationSurvivalRates;
    public HexagonClimateFoundationType[] climateSurvivalTypes;
    public float[] climateSurvivalRates;
    public EnvironmentalPropVariation[] variations;
    public PrefabType type;
    public float seed;
    public float density;
}