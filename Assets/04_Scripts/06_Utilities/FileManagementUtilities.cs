﻿using UnityEngine;
using System.IO;

// Utility static class for Data management
public static class FileManagementUtilities
{
    public static string GetDirectory()
    {
        string directory = Path.Combine(Application.persistentDataPath, "MapData");
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }
        return directory;
    }
}
