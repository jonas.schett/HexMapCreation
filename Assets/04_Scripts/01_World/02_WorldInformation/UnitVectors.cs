﻿using UnityEngine;

// a struct storing all unit vectors for the distances between hexagons
// just like in neighbours, this contains 8 vectors, as the orientation of the hexagon can vary
// it can either be facing north with a vertex or a side, changing the functionality of this struct
public struct UnitVectors
{
    public Vector3 FromSouth { get; private set; }
    public Vector3 FromSouthWest { get; private set; }
    public Vector3 FromWest { get; private set; }
    public Vector3 FromNorthWest { get; private set; }
    public Vector3 FromNorth { get; private set; }
    public Vector3 FromNorthEast { get; private set; }
    public Vector2 FromEast { get; private set; }
    public Vector3 FromSouthEast { get; private set; }

    public Vector3[] AsArray { get; private set; }

    // initialisation depends on the orientation of the hexagons
    public void Initialise(float hexSize, float innerRadius, bool vertexNorthOrientation = false)
    {
        if(!vertexNorthOrientation)
        {
            FromSouth = new Vector3(0, 0, 2f * innerRadius);
            FromSouthWest = new Vector3(1.5f * hexSize, 0, innerRadius);
            FromWest = new Vector3(2.0f * hexSize, 0, 0);
            FromNorthWest = new Vector3(1.5f * hexSize, 0, -innerRadius);
            FromNorth = -FromSouth;
            FromNorthEast = -FromSouthWest;
            FromEast = -FromWest;
            FromSouthEast = -FromNorthWest;

            AsArray = new Vector3[6] { FromSouthWest, FromNorthWest, FromNorth, FromNorthEast, FromSouthEast, FromSouth };
        }
        else
        {
            FromSouth = new Vector3(0, 0, 2f * hexSize);
            FromSouthWest = new Vector3(innerRadius, 0, 1.5f * hexSize);
            FromWest = new Vector3(2.0f * innerRadius, 0, 0);
            FromNorthWest = new Vector3(innerRadius, 0, -(1.5f * hexSize));
            FromNorth = -FromSouth;
            FromNorthEast = -FromSouthWest;
            FromEast = -FromWest;
            FromSouthEast = -FromNorthWest;

            AsArray = new Vector3[6] { FromSouthWest, FromWest, FromNorthWest, FromNorthEast, FromEast, FromSouthEast};
        }
        
    }
}
