﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using LPWAsset;

[System.Serializable]
public struct HexagonData
{
    public HexagonElevantionFoundationType ElevationFoundationType;
    public HexagonClimateFoundationType ClimateFoundationType;
    public HexagonLandscapeFoundationType LandscapeFoundationType;

    public GridInformation GridInfo;
}

[RequireComponent(typeof (MeshFilter),typeof(Transform))]
[ExecuteInEditMode]
public class Hexagon : MonoBehaviour
{
    #region Fields
    //vertex calculations
    public int FirstEdgeVertexIndex;
    public int HexagonIterations;

    // decides whether or not the sea will be active on this hexagon
    public bool AboveSeaLevel;

    // direct reference to the mesh used by this hexagon
    private Mesh _mesh;

    // different hexagon foundation types, determingin the ability
    // of props to exist on this particular hexagon
    // they also determine what noise is generated for the terrain
    public HexagonElevantionFoundationType ElevationFoundationType;
    public HexagonClimateFoundationType ClimateFoundationType;
    public HexagonLandscapeFoundationType LandscapeFoundationType;

    // Prop data and detail prop data
    // basically a list storing information about all the props attached to this hexagon
    private List<EnvironmentalPropData> LargePropData;
    private List<EnvironmentalPropData> DetailPropData;

    // Grid information about this hexagon, stores the absolute location,
    // grid location and possible needed offsets to other hexagons
    public GridInformation GridInfo;

    // Neighbours store the details about the 6 neighbouring hexagons
    public Neighbours Neighbours;

    // the water attached to the hexagon
    [SerializeField]
    private GameObject sea = null;

    [SerializeField]
    private GameObject grassParticleSystem;

    [SerializeField]
    private River river;

    private List<EnvironmentalProp> children;

    // difference is that the grass will only become active on hexagons that are actually next to the current one
    // and the current one itself
    private List<EnvironmentalProp> details;
    // bools for determining if the details are active, enabling or disabling
    // this is needed to prevent the hexagon from deactivating while there is still
    // a coroutine running
    private bool detailsActive;
    private bool detailsActivating;
    private bool detailsDeactivating;

    //River information
    private bool haveRiver;
    

    //bool telling other managers if the hexagon is currently active
    public bool Active = false;
    #endregion

    private void Awake()
    {
        LargePropData = new List<EnvironmentalPropData>();
        DetailPropData = new List<EnvironmentalPropData>();

        if (Application.isEditor)
        {
            MeshFilter mf = GetComponent<MeshFilter>();
            Mesh meshCopy = Instantiate(mf.sharedMesh) as Mesh;
            _mesh = mf.mesh = meshCopy;
        }
        else
        {
            _mesh = GetComponent<MeshFilter>().mesh;
        }

        if (sea != null)
        {
            sea.SetActive(false);
        }

        if (children == null)
        {
            children = new List<EnvironmentalProp>();
        }

        if(details == null)
        {
            details = new List<EnvironmentalProp>();
        }
    }

    #region Hexagon creation and data generation
    // this method is passing data from map generator to this particualr instance of a hexagon.
    public void GiveData(Hexagon[,] hexGrid, GridInformation gridInformation)
    {
        if(children == null)
        {
            children = new List<EnvironmentalProp>();
        }
        if(details == null)
        {
            details = new List<EnvironmentalProp>();
        }

        if (Application.isEditor)
        {
            MeshFilter mf = GetComponent<MeshFilter>();
            Mesh meshCopy = Instantiate(mf.sharedMesh) as Mesh;
            _mesh = mf.mesh = meshCopy;
        }
        else
        {
            _mesh = GetComponent<MeshFilter>().mesh;
        }

        GridInfo = gridInformation;

        Neighbours = new Neighbours(GridInfo.GridLocation, hexGrid, GridInfo.GridSize, GridInfo.VertexNorthOriented);
    }

    // Method for applying vertices to the hexagon that were calculated externally
    public void ApplyVertices(Vector3[] vertices)
    {
        _mesh.vertices = vertices;
    }

    // this method applies a mesh collider to the mesh of the hexagon
    public void AddCollider()
    {
        if(!gameObject.GetComponent<MeshCollider>())
        {
            gameObject.AddComponent<MeshCollider>();
        }
    }
    // Method for adding child objects to the hexagon
    public void AddChildObject(EnvironmentalProp child)
    {
        children.Add(child);
        child.transform.SetParent(transform);
    }
    //Method for adding Detials to the hexagon
    public void AddGrass(EnvironmentalProp grassChild)
    {
        details.Add(grassChild);
        grassChild.transform.SetParent(transform);
    }

    // this method simly recalculates mesh parameters
    public void Recalculate()
    {
        _mesh.RecalculateBounds();
        _mesh.RecalculateNormals();
        _mesh.RecalculateTangents();
    }
    public HexagonData GenerateHexagonData()
    {
        HexagonData data;
        data.ElevationFoundationType = ElevationFoundationType;
        data.LandscapeFoundationType = LandscapeFoundationType;
        data.ClimateFoundationType = ClimateFoundationType;
        data.GridInfo = GridInfo;
        return data;
    }
    #endregion

    #region Getters and Setters
    public Neighbours GetNeighbours()
    {
        return Neighbours;
    }
    public Vector3[] GetVertices()
    {
        return _mesh.vertices;
    }
    // Hexagon mesh related getter/setter
    public Mesh GetMesh()
    {
        return _mesh;
    }

    public void SetMesh(Mesh mesh)
    {
        _mesh = mesh;
    }
    // River related setter/getter
    public void SetRiver(Mesh mesh, QuadraticBezier curve, float width)
    {
        river.SetData(mesh, curve, width);
        haveRiver = true;
        SetRiverActive(haveRiver);
    }
    public River GetRiver()
    {
        return river;
    }
    public bool HasRiver()
    {
        return haveRiver;
    }
    // returns the absolute position on the grid
    public Vector3 GetAbsolutePosition()
    {
        return GridInfo.AbsoluteLocation;
    }

    public GridInformation GetGridInformation()
    {
        return GridInfo;
    }

    public Vector2 GetGridLocation()
    {
        return GridInfo.GridLocation;
    }
    #endregion

    #region Saving and Loading
    private static List<EnvironmentalProp> AccountForDeletedChildren(List<EnvironmentalProp> children)
    {
        List<EnvironmentalProp> noEmptyItems = new List<EnvironmentalProp>();
        foreach (var child in children)
        {
            if (child != null)
            {
                noEmptyItems.Add(child);
            }
        }
        return noEmptyItems;
    }
    public void SaveToFile(string directory)
    {
        children = AccountForDeletedChildren(children);
        details = AccountForDeletedChildren(details);

        string name = GridInfo.GridLocation.x.ToString().PadLeft(4,'0') + GridInfo.GridLocation.y.ToString().PadLeft(4,'0')+ ".hex";
        string path = Path.Combine(directory, name);
        BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create));

        writer.Write(_mesh.vertices.Length);
        writer.Write(_mesh.uv.Length);
        writer.Write(_mesh.colors.Length);

        writer.Write((uint)ElevationFoundationType);
        writer.Write((uint)LandscapeFoundationType);
        writer.Write((uint)ClimateFoundationType);

        writer.Write(HexagonIterations);
        writer.Write(FirstEdgeVertexIndex);

        writer.Write(AboveSeaLevel);

        foreach(var vertex in _mesh.vertices)
        {
            writer.Write(vertex.x);
            writer.Write(vertex.y);
            writer.Write(vertex.z);
        }

        foreach (var uv in _mesh.uv)
        {
            writer.Write(uv.x);
            writer.Write(uv.y);
        }

        foreach(var color in _mesh.colors)
        {
            writer.Write(color.r);
            writer.Write(color.g);
            writer.Write(color.b);
            writer.Write(color.a);
        }

        // also save positions of all child objects to file
        // first, we need to save the length of the children dictionary
        writer.Write(children.Count);
        foreach(var child in children)
        {
            writer.Write((int)child.Type);
            writer.Write(child.VariationID);
            writer.Write((int)child.SubType);
            // save the position and rotation of the object
            writer.Write(child.transform.localPosition.x);
            writer.Write(child.transform.localPosition.y);
            writer.Write(child.transform.localPosition.z);
            writer.Write(child.transform.localRotation.x);
            writer.Write(child.transform.localRotation.y);
            writer.Write(child.transform.localRotation.z);
            writer.Write(child.transform.localRotation.w);
        }

        // finally save positions of all the grass
        writer.Write(details.Count);
        foreach (var child in details)
        {
            writer.Write((int)child.Type);
            writer.Write(child.VariationID);
            writer.Write((int)child.SubType);
            // save the position and rotation of the object
            writer.Write(child.transform.localPosition.x);
            writer.Write(child.transform.localPosition.y);
            writer.Write(child.transform.localPosition.z);
            writer.Write(child.transform.localRotation.x);
            writer.Write(child.transform.localRotation.y);
            writer.Write(child.transform.localRotation.z);
            writer.Write(child.transform.localRotation.w);
        }

        writer.Write(haveRiver);
        if(haveRiver)
        {
            river.Save(writer);
        }

        writer.Close();
    }
    public void LoadFromFile(GridInformation gridInfo, string directory)
    {
        GridInfo = gridInfo;
        string name = gridInfo.GridLocation.x.ToString().PadLeft(4, '0') + gridInfo.GridLocation.y.ToString().PadLeft(4, '0') + ".hex";

        string path = Path.Combine(directory, name);
        BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open));

        int vertLen = reader.ReadInt32(); // would be vertexLength
        int uvLen = reader.ReadInt32(); // would be uvLength
        int colourLen = reader.ReadInt32(); // would be colorLengh

        Vector3[] vertices = new Vector3[vertLen];
        Vector2[] uvs = new Vector2[uvLen];
        Color[] colors = new Color[colourLen];

        // read types
        ElevationFoundationType = (HexagonElevantionFoundationType)reader.ReadUInt32();
        LandscapeFoundationType = (HexagonLandscapeFoundationType)reader.ReadUInt32();
        ClimateFoundationType = (HexagonClimateFoundationType)reader.ReadUInt32();

        HexagonIterations = reader.ReadInt32();
        FirstEdgeVertexIndex = reader.ReadInt32();

        AboveSeaLevel = reader.ReadBoolean();

        for (int i = 0; i < vertices.Length; i++)
        {
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            float z = reader.ReadSingle();
            vertices[i] = new Vector3(x, y, z);
        }

        for (int i = 0; i < uvs.Length; i++)
        {
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            uvs[i] = new Vector2(x, y);
        }

        for (int i = 0; i < colors.Length; i++)
        {
            float r = reader.ReadSingle();
            float g = reader.ReadSingle();
            float b = reader.ReadSingle();
            float a = reader.ReadSingle();
            colors[i] = new Color(r, g, b, a);
        }

        int childCount = reader.ReadInt32();
        for (int i = 0; i < childCount; i++)
        {
            PrefabType type = (PrefabType)reader.ReadInt32();
            int variationID = reader.ReadInt32();
            PrefabSubType subType = (PrefabSubType)reader.ReadInt32();
            // add this list to the types
            float posX = reader.ReadSingle();
            float posY = reader.ReadSingle();
            float posZ = reader.ReadSingle();
            float rotX = reader.ReadSingle();
            float rotY = reader.ReadSingle();
            float rotZ = reader.ReadSingle();
            float rotW = reader.ReadSingle();

            // instantiate the gameObject
            Vector3 offset = new Vector3(posX, posY, posZ);
            Quaternion rotation = new Quaternion(rotX, rotY, rotZ, rotW);
            EnvironmentalPropData prop = new EnvironmentalPropData(type, subType, variationID, offset, rotation);
            LargePropData.Add(prop);
        }

        int grassCount = reader.ReadInt32();
        for(int i = 0; i < grassCount; i ++)
        {
            PrefabType type = (PrefabType)reader.ReadInt32();
            int variationID = reader.ReadInt32();
            PrefabSubType subType = (PrefabSubType)reader.ReadInt32();
            // add this list to the types
            float posX = reader.ReadSingle();
            float posY = reader.ReadSingle();
            float posZ = reader.ReadSingle();
            float rotX = reader.ReadSingle();
            float rotY = reader.ReadSingle();
            float rotZ = reader.ReadSingle();
            float rotW = reader.ReadSingle();

            // instantiate the gameObject
            Vector3 offset = new Vector3(posX, posY, posZ);
            Quaternion rotation = new Quaternion(rotX, rotY, rotZ, rotW);
            EnvironmentalPropData prop = new EnvironmentalPropData(type, subType, variationID, offset, rotation);
            DetailPropData.Add(prop);
        }

        haveRiver = reader.ReadBoolean();
        if (haveRiver)
        {
            river.Load(reader);
            SetRiverActive(haveRiver);
        }

        reader.Close();
        _mesh.vertices = vertices;
        _mesh.colors = colors;
        _mesh.uv = uvs;
        
        _mesh.RecalculateBounds();
        _mesh.RecalculateNormals();
        _mesh.RecalculateTangents();

        var collider = GetComponent<MeshCollider>();
        if(collider)
        {
            collider.sharedMesh = _mesh;
        }

        //below code is for using a grass particle system if one exists
        if (grassParticleSystem != null)
        {
            ParticleSystem ps = grassParticleSystem.GetComponent<ParticleSystem>();
            if (ps != null)
            {
                var shape = ps.shape;
                shape.enabled = true;
                shape.shapeType = ParticleSystemShapeType.Mesh;
                shape.mesh = _mesh;
            }
        }
    }
    #endregion

    #region Activation and Deactivation
    public void SetSeaActive(bool a = false)
    {
        sea.SetActive(a);
    }
    public void Activate(EnvironmentalObjectFactoryManager propFactory)
    {
        Active = true;
        gameObject.SetActive(true);
        StartCoroutine(ActivateChildren(propFactory, LargePropData, children));
        SetSeaActive(!AboveSeaLevel);
        SetRiverActive(haveRiver);
    }
    public void Deactivate()
    {
        if (!detailsActive && !detailsActivating && !detailsDeactivating)
        {
            Active = false;
            StartCoroutine(DeactivateChildren());
        }
    }
    public void ActivateInstant(EnvironmentalObjectPlacer propSource)
    {
        Active = true;
        gameObject.SetActive(true);
        SetSeaActive(!AboveSeaLevel);

        for (int i = 0; i < LargePropData.Count; i++)
        {
            ActivatePropFromPropData(propSource.GetEnvironmentalObjectReferenceUtility(), LargePropData, i, children);
        }
        for (int i = 0; i < DetailPropData.Count; i++)
        {
            ActivatePropFromPropData(propSource.GetDetailObjectReferenceUtility(), DetailPropData, i, details);
        }
        detailsActive = true;
    }
    public void DeactivateInstant()
    {
        for (int i = 0; i < children.Count; i++)
        {
            children[i].Destroy();
        }
        children.Clear();
        for (int i = 0; i < details.Count; i++)
        {
            details[i].Destroy();
        }
        details.Clear();
        detailsActive = false;
        Active = false;
        gameObject.SetActive(false);
    }
    public void StartDetailActivation(EnvironmentalObjectFactoryManager propFactory)
    {
        if (!detailsActive && !detailsActivating && !detailsDeactivating)
        {
            StartCoroutine(ActivateDetails(propFactory, DetailPropData, details));
        }
    }
    public void StartDetailDeactivation()
    {
        if (detailsActive && !detailsActivating && !detailsDeactivating)
        {
            StartCoroutine(DeactivateDetails());
        }
    }
    IEnumerator ActivateDetails(EnvironmentalObjectFactoryManager propFactory, List<EnvironmentalPropData> propData, List<EnvironmentalProp> listToAddProps)
    {
        detailsActivating = true;
        for(int i = 0; i < propData.Count; i ++)
        {
            ActivatePropFromPropData(propFactory, propData, i, listToAddProps);
            if (i % 100 == 0)
            {
                yield return null;
            }
        }
        detailsActive = true;
        detailsActivating = false;
    }

    IEnumerator DeactivateDetails()
    {
        detailsDeactivating = true;
        for (int i = 0; i < details.Count; i++)
        {
            details[i].Destroy();
            if(i%100 == 0)
            {
                yield return null;
            }
        }
        details.Clear();

        detailsActive = false;
        detailsDeactivating = false;
    }
    IEnumerator ActivateChildren(EnvironmentalObjectFactoryManager propFactory, List<EnvironmentalPropData> propData, List<EnvironmentalProp> listToAddProps)
    {
        for(int i = 0; i < propData.Count; i ++)
        {
            ActivatePropFromPropData(propFactory, propData, i, listToAddProps);
            if (i % 100 == 0)
            {
                yield return null;
            }
        }
    }
    IEnumerator DeactivateChildren()
    {
        for (int i = 0; i < children.Count; i++)
        {
            children[i].Destroy();
            if (i % 100 == 0)
            {
                yield return null;
            }
        }
        children.Clear();
        gameObject.SetActive(false);
    }
    private void ActivatePropFromPropData(EnvironmentalObjectFactoryManager propFactory, List<EnvironmentalPropData> propData, int index, List<EnvironmentalProp> listToAddProps)
    {
        EnvironmentalPropData data = propData[index];
        EnvironmentalProp prop = propFactory.GetPrefab(data.type, data.subType, data.variationID, data);
        if (prop != null)
        {
            prop.transform.SetParent(transform);
            prop.transform.localPosition = data.offsetLocation;
            prop.transform.localRotation = data.localRotation;
            listToAddProps.Add(prop);
        }
    }

    public void SetRiverActive(bool active)
    {
        river.gameObject.SetActive(active);
    }
    #endregion
}
