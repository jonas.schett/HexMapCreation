﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

// An individual prop factory
//The factory has different queues for different subtypes
// it also has multiple variations of a single prefab
[System.Serializable]
public class EnvironmentalPropFactory
{
    public EnvironmentalPropFactory(EnvironmentalPropIdentifier propIdentifier)
    {
        myPropIdentifier = propIdentifier;
        type = propIdentifier.type;

    }
    public PrefabType type;
    public EnvironmentalPropIdentifier myPropIdentifier;
    // An array of Dictionaries holding a queue for each prefabType
    // The array indicates variations
    // The Dictionaries look at subtypes
    private Dictionary<PrefabSubType, Queue<EnvironmentalProp>>[] pools;

    #region Setup and Teardown
    private void CreatePools()
    {
        // create subType pools
        pools = new Dictionary<PrefabSubType, Queue<EnvironmentalProp>>[myPropIdentifier.variations.Length];
        for (int i = 0; i < pools.Length; i++)
        {
            pools[i] = new Dictionary<PrefabSubType, Queue<EnvironmentalProp>>();
            foreach (var subType in myPropIdentifier.variations[i].subTypes)
            {
                if (!pools[i].ContainsKey(subType.subType))
                {
                    pools[i].Add(subType.subType, new Queue<EnvironmentalProp>());
                }
            }
        }
    }
    internal void ClearAll()
    {
        if (pools != null)
        {
            foreach (var pool in pools)
            {
                foreach (var queue in pool)
                {
                    while (queue.Value.Count > 0)
                    {
                        var item = queue.Value.Dequeue();
                        if (item != null && item.gameObject != null)
                        {
#if (UNITY_EDITOR)
                            Object.DestroyImmediate(item.gameObject);
#else
                            Object.Destroy(item.gameObject);
#endif
                        }
                    }
                    // clear the queue
                    queue.Value.Clear();
                }

                // we don't want to clear the pool, as it's queues can be reused later
            }
        }
    }
    #endregion

    #region Prop retrieval and returning
    // function for retrieving a prop, it will look with the variationID and subtype to find a suitable prop to return
    // Should the queue of the specified varietyIndex and subtype not hold any props, a new one will be created
    // as soon as it is returned, it will be added to the relevant queue
    public EnvironmentalProp GetProp(int variationIndex = -1, PrefabSubType subType = PrefabSubType.Standard, EnvironmentalPropData data = null)
    {
        if (pools == null)
        {
            CreatePools();
        }

        if (!((variationIndex < pools.Length) && (variationIndex < myPropIdentifier.variations.Length)))
        {
            Debug.LogError("We the requested variation index is larger than the provided number of variations");
            return null;
        }

        // we are below 0, so we pick a random variation
        if (variationIndex < 0)
        {
            variationIndex = Random.Range(0, pools.Length - 1);
        }

        EnvironmentalProp prop;
        Dictionary<PrefabSubType, Queue<EnvironmentalProp>> pool = pools[variationIndex];

        if(subType == PrefabSubType.RANDOM)
        {
            List <PrefabSubType> keys = pool.Keys.ToList();
            subType = keys[Random.Range(0, keys.Count - 1)];
        }
        else if (!pool.ContainsKey(subType))
        {
            if (!pool.ContainsKey(PrefabSubType.Standard))
            {
                // error
                Debug.LogError("pool does not contain a prefab for the defined identifier or a standard identifier");
                return null;
            }

            Debug.LogWarning("Pool does not contain a prefab for the defined identifier '" + subType + "', returning a standard prefab instead");
            subType = PrefabSubType.Standard;
        }

        Queue<EnvironmentalProp> subPool = pool[subType];

        // if the prefab exists in the current pool, that is great, we just need to retrieve it
        if (subPool.Count > 0)
        {
            prop = subPool.Dequeue();
            prop.gameObject.SetActive(true);
            return prop;
        }
        // if it does not exist, we need to create it
        else
        {
            // get correct index
            EnvironmentalPropVariation prefab = myPropIdentifier.variations[variationIndex];

            EnvironmentalPropSubType subPrefab = prefab.subTypes.FirstOrDefault(p => p.subType == subType);

            if (subPrefab.subPrefab != null)
            {
                prop = Object.Instantiate(subPrefab.subPrefab).AddComponent<EnvironmentalProp>();
                if(data != null)
                {
                    prop.Data = data;
                }
                else
                {
                    prop.Data = new EnvironmentalPropData(type, subType, variationIndex);
                }
                prop.Manager = this;
                return prop;
            }
            else
            {
                Debug.LogError("No prefab was attached to the defined prop");
            }
        }

        Debug.LogError("Unknown error, returning null");
        return null;
    }

    public void ReturnPrefab(EnvironmentalProp prop)
    {
        // add the prop to the queue for reusing
        pools[prop.Data.variationID][prop.SubType].Enqueue(prop);
        prop.transform.SetParent(null);
        prop.gameObject.SetActive(false);
    }
    #endregion
}