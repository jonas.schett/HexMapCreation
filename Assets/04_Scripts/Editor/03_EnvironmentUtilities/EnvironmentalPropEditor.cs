﻿#if (UNITY_EDITOR)
using System.IO;
using UnityEditor;
using UnityEngine;

public class EnvironmentalPropEditor : EditorWindow
{

    [MenuItem("Tools/Clone Scriptable Object")]
    static void CloneScriptableObject()
    {
        Object obj = Selection.objects[0];
        EnvironmentalPropIdentifier clone = Object.Instantiate(obj) as EnvironmentalPropIdentifier;
        Debug.Log(clone.elevationSurvivalTypes);
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "clone.asset");
        AssetDatabase.CreateAsset(clone, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
    }
}
 
public static class ScriptableObjectUtility
{
    /// <summary>
    //  This makes it easy to create, name and place unique new ScriptableObject asset files.
    /// </summary>
    public static void CreateAsset<T>() where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;

    }

    public static void OnDestroy()
    {

        Debug.Log("SOU on Destory");
        AssetDatabase.SaveAssets();

    }

    public static void OnApplicationQuit()
    {
        Debug.Log("SOU on Quit");
        AssetDatabase.SaveAssets();
    }
}
#endif
