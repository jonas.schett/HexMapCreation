﻿using UnityEngine;

// Basic player controller
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{

    public float speed;

    private Rigidbody rb;

    public Camera mainCamera;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // move the player
    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        movement = mainCamera.transform.TransformDirection(movement);

        rb.AddForce(movement * speed);

        if (Input.GetKeyDown("space"))
        {
            rb.AddForce(Vector3.up * 500f);
        }
            
        
    }
}